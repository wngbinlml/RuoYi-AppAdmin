package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.SearchRec;
import java.util.List;

/**
 * 搜索记录Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface SearchRecMapper 
{
    /**
     * 查询搜索记录
     * 
     * @param searchId 搜索记录ID
     * @return 搜索记录
     */
    public SearchRec selectSearchRecById(Long searchId);

    /**
     * 查询搜索记录列表
     * 
     * @param searchRec 搜索记录
     * @return 搜索记录集合
     */
    public List<SearchRec> selectSearchRecList(SearchRec searchRec);

    /**
     * 新增搜索记录
     * 
     * @param searchRec 搜索记录
     * @return 结果
     */
    public int insertSearchRec(SearchRec searchRec);

    /**
     * 修改搜索记录
     * 
     * @param searchRec 搜索记录
     * @return 结果
     */
    public int updateSearchRec(SearchRec searchRec);

    /**
     * 删除搜索记录
     * 
     * @param searchId 搜索记录ID
     * @return 结果
     */
    public int deleteSearchRecById(Long searchId);

    /**
     * 批量删除搜索记录
     * 
     * @param searchIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSearchRecByIds(String[] searchIds);
}
