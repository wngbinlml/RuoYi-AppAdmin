package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.Focus;
import java.util.List;

/**
 * 关注Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface FocusMapper 
{
    /**
     * 查询关注
     * 
     * @param focusId 关注ID
     * @return 关注
     */
    public Focus selectFocusById(Long focusId);

    /**
     * 查询关注列表
     * 
     * @param focus 关注
     * @return 关注集合
     */
    public List<Focus> selectFocusList(Focus focus);

    /**
     * 新增关注
     * 
     * @param focus 关注
     * @return 结果
     */
    public int insertFocus(Focus focus);

    /**
     * 修改关注
     * 
     * @param focus 关注
     * @return 结果
     */
    public int updateFocus(Focus focus);

    /**
     * 删除关注
     * 
     * @param focusId 关注ID
     * @return 结果
     */
    public int deleteFocusById(Long focusId);

    /**
     * 批量删除关注
     * 
     * @param focusIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteFocusByIds(String[] focusIds);
}
