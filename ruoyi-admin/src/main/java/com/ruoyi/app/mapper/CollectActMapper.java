package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.CollectAct;
import java.util.List;

/**
 * 收藏活动Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface CollectActMapper 
{
    /**
     * 查询收藏活动
     * 
     * @param collectId 收藏活动ID
     * @return 收藏活动
     */
    public CollectAct selectCollectActById(Long collectId);

    /**
     * 查询收藏活动列表
     * 
     * @param collectAct 收藏活动
     * @return 收藏活动集合
     */
    public List<CollectAct> selectCollectActList(CollectAct collectAct);

    /**
     * 新增收藏活动
     * 
     * @param collectAct 收藏活动
     * @return 结果
     */
    public int insertCollectAct(CollectAct collectAct);

    /**
     * 修改收藏活动
     * 
     * @param collectAct 收藏活动
     * @return 结果
     */
    public int updateCollectAct(CollectAct collectAct);

    /**
     * 删除收藏活动
     * 
     * @param collectId 收藏活动ID
     * @return 结果
     */
    public int deleteCollectActById(Long collectId);

    /**
     * 批量删除收藏活动
     * 
     * @param collectIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteCollectActByIds(String[] collectIds);
}
