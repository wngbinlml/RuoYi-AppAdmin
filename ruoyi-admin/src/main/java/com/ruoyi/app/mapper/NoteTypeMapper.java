package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.NoteType;
import java.util.List;

/**
 * 游记类型Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface NoteTypeMapper 
{
    /**
     * 查询游记类型
     * 
     * @param noteTypeId 游记类型ID
     * @return 游记类型
     */
    public NoteType selectNoteTypeById(Long noteTypeId);

    /**
     * 查询游记类型列表
     * 
     * @param noteType 游记类型
     * @return 游记类型集合
     */
    public List<NoteType> selectNoteTypeList(NoteType noteType);

    /**
     * 新增游记类型
     * 
     * @param noteType 游记类型
     * @return 结果
     */
    public int insertNoteType(NoteType noteType);

    /**
     * 修改游记类型
     * 
     * @param noteType 游记类型
     * @return 结果
     */
    public int updateNoteType(NoteType noteType);

    /**
     * 删除游记类型
     * 
     * @param noteTypeId 游记类型ID
     * @return 结果
     */
    public int deleteNoteTypeById(Long noteTypeId);

    /**
     * 批量删除游记类型
     * 
     * @param noteTypeIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteNoteTypeByIds(String[] noteTypeIds);
}
