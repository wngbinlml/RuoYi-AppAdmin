package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.PlazaContent;
import java.util.List;

/**
 * 广场动态Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface PlazaContentMapper 
{
    /**
     * 查询广场动态
     * 
     * @param contentId 广场动态ID
     * @return 广场动态
     */
    public PlazaContent selectPlazaContentById(String contentId);

    /**
     * 查询广场动态列表
     * 
     * @param plazaContent 广场动态
     * @return 广场动态集合
     */
    public List<PlazaContent> selectPlazaContentList(PlazaContent plazaContent);

    /**
     * 新增广场动态
     * 
     * @param plazaContent 广场动态
     * @return 结果
     */
    public int insertPlazaContent(PlazaContent plazaContent);

    /**
     * 修改广场动态
     * 
     * @param plazaContent 广场动态
     * @return 结果
     */
    public int updatePlazaContent(PlazaContent plazaContent);

    /**
     * 删除广场动态
     * 
     * @param contentId 广场动态ID
     * @return 结果
     */
    public int deletePlazaContentById(String contentId);

    /**
     * 批量删除广场动态
     * 
     * @param contentIds 需要删除的数据ID
     * @return 结果
     */
    public int deletePlazaContentByIds(String[] contentIds);
}
