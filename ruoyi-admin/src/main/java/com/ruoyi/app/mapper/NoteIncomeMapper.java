package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.NoteIncome;
import java.util.List;

/**
 * 游记收入Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface NoteIncomeMapper 
{
    /**
     * 查询游记收入
     * 
     * @param noteIncomeId 游记收入ID
     * @return 游记收入
     */
    public NoteIncome selectNoteIncomeById(String noteIncomeId);

    /**
     * 查询游记收入列表
     * 
     * @param noteIncome 游记收入
     * @return 游记收入集合
     */
    public List<NoteIncome> selectNoteIncomeList(NoteIncome noteIncome);

    /**
     * 新增游记收入
     * 
     * @param noteIncome 游记收入
     * @return 结果
     */
    public int insertNoteIncome(NoteIncome noteIncome);

    /**
     * 修改游记收入
     * 
     * @param noteIncome 游记收入
     * @return 结果
     */
    public int updateNoteIncome(NoteIncome noteIncome);

    /**
     * 删除游记收入
     * 
     * @param noteIncomeId 游记收入ID
     * @return 结果
     */
    public int deleteNoteIncomeById(String noteIncomeId);

    /**
     * 批量删除游记收入
     * 
     * @param noteIncomeIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteNoteIncomeByIds(String[] noteIncomeIds);
}
