package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.LevelInfo;
import java.util.List;

/**
 * 等级Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface LevelInfoMapper 
{
    /**
     * 查询等级
     * 
     * @param levelId 等级ID
     * @return 等级
     */
    public LevelInfo selectLevelInfoById(String levelId);

    /**
     * 查询等级列表
     * 
     * @param levelInfo 等级
     * @return 等级集合
     */
    public List<LevelInfo> selectLevelInfoList(LevelInfo levelInfo);

    /**
     * 新增等级
     * 
     * @param levelInfo 等级
     * @return 结果
     */
    public int insertLevelInfo(LevelInfo levelInfo);

    /**
     * 修改等级
     * 
     * @param levelInfo 等级
     * @return 结果
     */
    public int updateLevelInfo(LevelInfo levelInfo);

    /**
     * 删除等级
     * 
     * @param levelId 等级ID
     * @return 结果
     */
    public int deleteLevelInfoById(String levelId);

    /**
     * 批量删除等级
     * 
     * @param levelIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteLevelInfoByIds(String[] levelIds);
}
