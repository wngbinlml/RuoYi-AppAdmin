package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.FaceRec;
import java.util.List;

/**
 * 关注Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface FaceRecMapper 
{
    /**
     * 查询关注
     * 
     * @param faceId 关注ID
     * @return 关注
     */
    public FaceRec selectFaceRecById(String faceId);

    /**
     * 查询关注列表
     * 
     * @param faceRec 关注
     * @return 关注集合
     */
    public List<FaceRec> selectFaceRecList(FaceRec faceRec);

    /**
     * 新增关注
     * 
     * @param faceRec 关注
     * @return 结果
     */
    public int insertFaceRec(FaceRec faceRec);

    /**
     * 修改关注
     * 
     * @param faceRec 关注
     * @return 结果
     */
    public int updateFaceRec(FaceRec faceRec);

    /**
     * 删除关注
     * 
     * @param faceId 关注ID
     * @return 结果
     */
    public int deleteFaceRecById(String faceId);

    /**
     * 批量删除关注
     * 
     * @param faceIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaceRecByIds(String[] faceIds);
}
