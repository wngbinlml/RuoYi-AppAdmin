package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.PlazaNofeelTag;
import java.util.List;

/**
 * 广场不感兴趣Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface PlazaNofeelTagMapper 
{
    /**
     * 查询广场不感兴趣
     * 
     * @param nofeelId 广场不感兴趣ID
     * @return 广场不感兴趣
     */
    public PlazaNofeelTag selectPlazaNofeelTagById(String nofeelId);

    /**
     * 查询广场不感兴趣列表
     * 
     * @param plazaNofeelTag 广场不感兴趣
     * @return 广场不感兴趣集合
     */
    public List<PlazaNofeelTag> selectPlazaNofeelTagList(PlazaNofeelTag plazaNofeelTag);

    /**
     * 新增广场不感兴趣
     * 
     * @param plazaNofeelTag 广场不感兴趣
     * @return 结果
     */
    public int insertPlazaNofeelTag(PlazaNofeelTag plazaNofeelTag);

    /**
     * 修改广场不感兴趣
     * 
     * @param plazaNofeelTag 广场不感兴趣
     * @return 结果
     */
    public int updatePlazaNofeelTag(PlazaNofeelTag plazaNofeelTag);

    /**
     * 删除广场不感兴趣
     * 
     * @param nofeelId 广场不感兴趣ID
     * @return 结果
     */
    public int deletePlazaNofeelTagById(String nofeelId);

    /**
     * 批量删除广场不感兴趣
     * 
     * @param nofeelIds 需要删除的数据ID
     * @return 结果
     */
    public int deletePlazaNofeelTagByIds(String[] nofeelIds);
}
