package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.NoteIncomeMapper;
import com.ruoyi.app.domain.NoteIncome;
import com.ruoyi.app.service.INoteIncomeService;
import com.ruoyi.common.core.text.Convert;

/**
 * 游记收入Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class NoteIncomeServiceImpl implements INoteIncomeService 
{
    @Autowired
    private NoteIncomeMapper noteIncomeMapper;

    /**
     * 查询游记收入
     * 
     * @param noteIncomeId 游记收入ID
     * @return 游记收入
     */
    @Override
    public NoteIncome selectNoteIncomeById(String noteIncomeId)
    {
        return noteIncomeMapper.selectNoteIncomeById(noteIncomeId);
    }

    /**
     * 查询游记收入列表
     * 
     * @param noteIncome 游记收入
     * @return 游记收入
     */
    @Override
    public List<NoteIncome> selectNoteIncomeList(NoteIncome noteIncome)
    {
        return noteIncomeMapper.selectNoteIncomeList(noteIncome);
    }

    /**
     * 新增游记收入
     * 
     * @param noteIncome 游记收入
     * @return 结果
     */
    @Override
    public int insertNoteIncome(NoteIncome noteIncome)
    {
        noteIncome.setCreateTime(DateUtils.getNowDate());
        return noteIncomeMapper.insertNoteIncome(noteIncome);
    }

    /**
     * 修改游记收入
     * 
     * @param noteIncome 游记收入
     * @return 结果
     */
    @Override
    public int updateNoteIncome(NoteIncome noteIncome)
    {
        return noteIncomeMapper.updateNoteIncome(noteIncome);
    }

    /**
     * 删除游记收入对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteNoteIncomeByIds(String ids)
    {
        return noteIncomeMapper.deleteNoteIncomeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除游记收入信息
     * 
     * @param noteIncomeId 游记收入ID
     * @return 结果
     */
    @Override
    public int deleteNoteIncomeById(String noteIncomeId)
    {
        return noteIncomeMapper.deleteNoteIncomeById(noteIncomeId);
    }
}
