package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.PlazaCommentMapper;
import com.ruoyi.app.domain.PlazaComment;
import com.ruoyi.app.service.IPlazaCommentService;
import com.ruoyi.common.core.text.Convert;

/**
 * 广场评论Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class PlazaCommentServiceImpl implements IPlazaCommentService 
{
    @Autowired
    private PlazaCommentMapper plazaCommentMapper;

    /**
     * 查询广场评论
     * 
     * @param plazaCommId 广场评论ID
     * @return 广场评论
     */
    @Override
    public PlazaComment selectPlazaCommentById(String plazaCommId)
    {
        return plazaCommentMapper.selectPlazaCommentById(plazaCommId);
    }

    /**
     * 查询广场评论列表
     * 
     * @param plazaComment 广场评论
     * @return 广场评论
     */
    @Override
    public List<PlazaComment> selectPlazaCommentList(PlazaComment plazaComment)
    {
        return plazaCommentMapper.selectPlazaCommentList(plazaComment);
    }

    /**
     * 新增广场评论
     * 
     * @param plazaComment 广场评论
     * @return 结果
     */
    @Override
    public int insertPlazaComment(PlazaComment plazaComment)
    {
        plazaComment.setCreateTime(DateUtils.getNowDate());
        return plazaCommentMapper.insertPlazaComment(plazaComment);
    }

    /**
     * 修改广场评论
     * 
     * @param plazaComment 广场评论
     * @return 结果
     */
    @Override
    public int updatePlazaComment(PlazaComment plazaComment)
    {
        return plazaCommentMapper.updatePlazaComment(plazaComment);
    }

    /**
     * 删除广场评论对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePlazaCommentByIds(String ids)
    {
        return plazaCommentMapper.deletePlazaCommentByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除广场评论信息
     * 
     * @param plazaCommId 广场评论ID
     * @return 结果
     */
    @Override
    public int deletePlazaCommentById(String plazaCommId)
    {
        return plazaCommentMapper.deletePlazaCommentById(plazaCommId);
    }
}
