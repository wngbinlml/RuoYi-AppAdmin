package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.ReportMapper;
import com.ruoyi.app.domain.Report;
import com.ruoyi.app.service.IReportService;
import com.ruoyi.common.core.text.Convert;

/**
 * 举报Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class ReportServiceImpl implements IReportService 
{
    @Autowired
    private ReportMapper reportMapper;

    /**
     * 查询举报
     * 
     * @param reportId 举报ID
     * @return 举报
     */
    @Override
    public Report selectReportById(Long reportId)
    {
        return reportMapper.selectReportById(reportId);
    }

    /**
     * 查询举报列表
     * 
     * @param report 举报
     * @return 举报
     */
    @Override
    public List<Report> selectReportList(Report report)
    {
        return reportMapper.selectReportList(report);
    }

    /**
     * 新增举报
     * 
     * @param report 举报
     * @return 结果
     */
    @Override
    public int insertReport(Report report)
    {
        report.setCreateTime(DateUtils.getNowDate());
        return reportMapper.insertReport(report);
    }

    /**
     * 修改举报
     * 
     * @param report 举报
     * @return 结果
     */
    @Override
    public int updateReport(Report report)
    {
        return reportMapper.updateReport(report);
    }

    /**
     * 删除举报对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteReportByIds(String ids)
    {
        return reportMapper.deleteReportByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除举报信息
     * 
     * @param reportId 举报ID
     * @return 结果
     */
    @Override
    public int deleteReportById(Long reportId)
    {
        return reportMapper.deleteReportById(reportId);
    }
}
