package com.ruoyi.app.service;

import com.ruoyi.app.domain.PkRec;
import java.util.List;

/**
 * pk比赛Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface IPkRecService 
{
    /**
     * 查询pk比赛
     * 
     * @param pkId pk比赛ID
     * @return pk比赛
     */
    public PkRec selectPkRecById(String pkId);

    /**
     * 查询pk比赛列表
     * 
     * @param pkRec pk比赛
     * @return pk比赛集合
     */
    public List<PkRec> selectPkRecList(PkRec pkRec);

    /**
     * 新增pk比赛
     * 
     * @param pkRec pk比赛
     * @return 结果
     */
    public int insertPkRec(PkRec pkRec);

    /**
     * 修改pk比赛
     * 
     * @param pkRec pk比赛
     * @return 结果
     */
    public int updatePkRec(PkRec pkRec);

    /**
     * 批量删除pk比赛
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePkRecByIds(String ids);

    /**
     * 删除pk比赛信息
     * 
     * @param pkId pk比赛ID
     * @return 结果
     */
    public int deletePkRecById(String pkId);
}
