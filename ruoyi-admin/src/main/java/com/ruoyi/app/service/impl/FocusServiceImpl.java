package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.FocusMapper;
import com.ruoyi.app.domain.Focus;
import com.ruoyi.app.service.IFocusService;
import com.ruoyi.common.core.text.Convert;

/**
 * 关注Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class FocusServiceImpl implements IFocusService 
{
    @Autowired
    private FocusMapper focusMapper;

    /**
     * 查询关注
     * 
     * @param focusId 关注ID
     * @return 关注
     */
    @Override
    public Focus selectFocusById(Long focusId)
    {
        return focusMapper.selectFocusById(focusId);
    }

    /**
     * 查询关注列表
     * 
     * @param focus 关注
     * @return 关注
     */
    @Override
    public List<Focus> selectFocusList(Focus focus)
    {
        return focusMapper.selectFocusList(focus);
    }

    /**
     * 新增关注
     * 
     * @param focus 关注
     * @return 结果
     */
    @Override
    public int insertFocus(Focus focus)
    {
        focus.setCreateTime(DateUtils.getNowDate());
        return focusMapper.insertFocus(focus);
    }

    /**
     * 修改关注
     * 
     * @param focus 关注
     * @return 结果
     */
    @Override
    public int updateFocus(Focus focus)
    {
        return focusMapper.updateFocus(focus);
    }

    /**
     * 删除关注对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteFocusByIds(String ids)
    {
        return focusMapper.deleteFocusByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除关注信息
     * 
     * @param focusId 关注ID
     * @return 结果
     */
    @Override
    public int deleteFocusById(Long focusId)
    {
        return focusMapper.deleteFocusById(focusId);
    }
}
