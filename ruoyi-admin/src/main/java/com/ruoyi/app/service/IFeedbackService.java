package com.ruoyi.app.service;

import com.ruoyi.app.domain.Feedback;
import java.util.List;

/**
 * 意见反馈Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface IFeedbackService 
{
    /**
     * 查询意见反馈
     * 
     * @param feedId 意见反馈ID
     * @return 意见反馈
     */
    public Feedback selectFeedbackById(String feedId);

    /**
     * 查询意见反馈列表
     * 
     * @param feedback 意见反馈
     * @return 意见反馈集合
     */
    public List<Feedback> selectFeedbackList(Feedback feedback);

    /**
     * 新增意见反馈
     * 
     * @param feedback 意见反馈
     * @return 结果
     */
    public int insertFeedback(Feedback feedback);

    /**
     * 修改意见反馈
     * 
     * @param feedback 意见反馈
     * @return 结果
     */
    public int updateFeedback(Feedback feedback);

    /**
     * 批量删除意见反馈
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFeedbackByIds(String ids);

    /**
     * 删除意见反馈信息
     * 
     * @param feedId 意见反馈ID
     * @return 结果
     */
    public int deleteFeedbackById(String feedId);
}
