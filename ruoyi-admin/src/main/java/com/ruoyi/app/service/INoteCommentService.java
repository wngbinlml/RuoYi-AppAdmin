package com.ruoyi.app.service;

import com.ruoyi.app.domain.NoteComment;
import java.util.List;

/**
 * 游记评论Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface INoteCommentService 
{
    /**
     * 查询游记评论
     * 
     * @param noteId 游记评论ID
     * @return 游记评论
     */
    public NoteComment selectNoteCommentById(String noteId);

    /**
     * 查询游记评论列表
     * 
     * @param noteComment 游记评论
     * @return 游记评论集合
     */
    public List<NoteComment> selectNoteCommentList(NoteComment noteComment);

    /**
     * 新增游记评论
     * 
     * @param noteComment 游记评论
     * @return 结果
     */
    public int insertNoteComment(NoteComment noteComment);

    /**
     * 修改游记评论
     * 
     * @param noteComment 游记评论
     * @return 结果
     */
    public int updateNoteComment(NoteComment noteComment);

    /**
     * 批量删除游记评论
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteNoteCommentByIds(String ids);

    /**
     * 删除游记评论信息
     * 
     * @param noteId 游记评论ID
     * @return 结果
     */
    public int deleteNoteCommentById(String noteId);
}
