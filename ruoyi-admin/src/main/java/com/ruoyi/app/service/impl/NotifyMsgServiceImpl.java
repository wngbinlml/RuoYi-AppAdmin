package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.NotifyMsgMapper;
import com.ruoyi.app.domain.NotifyMsg;
import com.ruoyi.app.service.INotifyMsgService;
import com.ruoyi.common.core.text.Convert;

/**
 * 通知Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class NotifyMsgServiceImpl implements INotifyMsgService 
{
    @Autowired
    private NotifyMsgMapper notifyMsgMapper;

    /**
     * 查询通知
     * 
     * @param notifyId 通知ID
     * @return 通知
     */
    @Override
    public NotifyMsg selectNotifyMsgById(String notifyId)
    {
        return notifyMsgMapper.selectNotifyMsgById(notifyId);
    }

    /**
     * 查询通知列表
     * 
     * @param notifyMsg 通知
     * @return 通知
     */
    @Override
    public List<NotifyMsg> selectNotifyMsgList(NotifyMsg notifyMsg)
    {
        return notifyMsgMapper.selectNotifyMsgList(notifyMsg);
    }

    /**
     * 新增通知
     * 
     * @param notifyMsg 通知
     * @return 结果
     */
    @Override
    public int insertNotifyMsg(NotifyMsg notifyMsg)
    {
        notifyMsg.setCreateTime(DateUtils.getNowDate());
        return notifyMsgMapper.insertNotifyMsg(notifyMsg);
    }

    /**
     * 修改通知
     * 
     * @param notifyMsg 通知
     * @return 结果
     */
    @Override
    public int updateNotifyMsg(NotifyMsg notifyMsg)
    {
        return notifyMsgMapper.updateNotifyMsg(notifyMsg);
    }

    /**
     * 删除通知对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteNotifyMsgByIds(String ids)
    {
        return notifyMsgMapper.deleteNotifyMsgByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除通知信息
     * 
     * @param notifyId 通知ID
     * @return 结果
     */
    @Override
    public int deleteNotifyMsgById(String notifyId)
    {
        return notifyMsgMapper.deleteNotifyMsgById(notifyId);
    }
}
