package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.ActivityMapper;
import com.ruoyi.app.domain.Activity;
import com.ruoyi.app.service.IActivityService;
import com.ruoyi.common.core.text.Convert;

/**
 * 活动Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class ActivityServiceImpl implements IActivityService 
{
    @Autowired
    private ActivityMapper activityMapper;

    /**
     * 查询活动
     * 
     * @param actId 活动ID
     * @return 活动
     */
    @Override
    public Activity selectActivityById(String actId)
    {
        return activityMapper.selectActivityById(actId);
    }

    /**
     * 查询活动列表
     * 
     * @param activity 活动
     * @return 活动
     */
    @Override
    public List<Activity> selectActivityList(Activity activity)
    {
        return activityMapper.selectActivityList(activity);
    }

    /**
     * 新增活动
     * 
     * @param activity 活动
     * @return 结果
     */
    @Override
    public int insertActivity(Activity activity)
    {
        activity.setCreateTime(DateUtils.getNowDate());
        return activityMapper.insertActivity(activity);
    }

    /**
     * 修改活动
     * 
     * @param activity 活动
     * @return 结果
     */
    @Override
    public int updateActivity(Activity activity)
    {
        return activityMapper.updateActivity(activity);
    }

    /**
     * 删除活动对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteActivityByIds(String ids)
    {
        return activityMapper.deleteActivityByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除活动信息
     * 
     * @param actId 活动ID
     * @return 结果
     */
    @Override
    public int deleteActivityById(String actId)
    {
        return activityMapper.deleteActivityById(actId);
    }
}
