package com.ruoyi.app.service;

import com.ruoyi.app.domain.VerifyRec;
import java.util.List;

/**
 * 验证码记录Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface IVerifyRecService 
{
    /**
     * 查询验证码记录
     * 
     * @param verifyId 验证码记录ID
     * @return 验证码记录
     */
    public VerifyRec selectVerifyRecById(String verifyId);

    /**
     * 查询验证码记录列表
     * 
     * @param verifyRec 验证码记录
     * @return 验证码记录集合
     */
    public List<VerifyRec> selectVerifyRecList(VerifyRec verifyRec);

    /**
     * 新增验证码记录
     * 
     * @param verifyRec 验证码记录
     * @return 结果
     */
    public int insertVerifyRec(VerifyRec verifyRec);

    /**
     * 修改验证码记录
     * 
     * @param verifyRec 验证码记录
     * @return 结果
     */
    public int updateVerifyRec(VerifyRec verifyRec);

    /**
     * 批量删除验证码记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteVerifyRecByIds(String ids);

    /**
     * 删除验证码记录信息
     * 
     * @param verifyId 验证码记录ID
     * @return 结果
     */
    public int deleteVerifyRecById(String verifyId);
}
