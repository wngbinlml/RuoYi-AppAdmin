package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.FaceRecMapper;
import com.ruoyi.app.domain.FaceRec;
import com.ruoyi.app.service.IFaceRecService;
import com.ruoyi.common.core.text.Convert;

/**
 * 关注Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class FaceRecServiceImpl implements IFaceRecService 
{
    @Autowired
    private FaceRecMapper faceRecMapper;

    /**
     * 查询关注
     * 
     * @param faceId 关注ID
     * @return 关注
     */
    @Override
    public FaceRec selectFaceRecById(String faceId)
    {
        return faceRecMapper.selectFaceRecById(faceId);
    }

    /**
     * 查询关注列表
     * 
     * @param faceRec 关注
     * @return 关注
     */
    @Override
    public List<FaceRec> selectFaceRecList(FaceRec faceRec)
    {
        return faceRecMapper.selectFaceRecList(faceRec);
    }

    /**
     * 新增关注
     * 
     * @param faceRec 关注
     * @return 结果
     */
    @Override
    public int insertFaceRec(FaceRec faceRec)
    {
        faceRec.setCreateTime(DateUtils.getNowDate());
        return faceRecMapper.insertFaceRec(faceRec);
    }

    /**
     * 修改关注
     * 
     * @param faceRec 关注
     * @return 结果
     */
    @Override
    public int updateFaceRec(FaceRec faceRec)
    {
        return faceRecMapper.updateFaceRec(faceRec);
    }

    /**
     * 删除关注对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteFaceRecByIds(String ids)
    {
        return faceRecMapper.deleteFaceRecByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除关注信息
     * 
     * @param faceId 关注ID
     * @return 结果
     */
    @Override
    public int deleteFaceRecById(String faceId)
    {
        return faceRecMapper.deleteFaceRecById(faceId);
    }
}
