package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.MemberBlacklistMapper;
import com.ruoyi.app.domain.MemberBlacklist;
import com.ruoyi.app.service.IMemberBlacklistService;
import com.ruoyi.common.core.text.Convert;

/**
 * 会员黑名单Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class MemberBlacklistServiceImpl implements IMemberBlacklistService 
{
    @Autowired
    private MemberBlacklistMapper memberBlacklistMapper;

    /**
     * 查询会员黑名单
     * 
     * @param blackId 会员黑名单ID
     * @return 会员黑名单
     */
    @Override
    public MemberBlacklist selectMemberBlacklistById(Long blackId)
    {
        return memberBlacklistMapper.selectMemberBlacklistById(blackId);
    }

    /**
     * 查询会员黑名单列表
     * 
     * @param memberBlacklist 会员黑名单
     * @return 会员黑名单
     */
    @Override
    public List<MemberBlacklist> selectMemberBlacklistList(MemberBlacklist memberBlacklist)
    {
        return memberBlacklistMapper.selectMemberBlacklistList(memberBlacklist);
    }

    /**
     * 新增会员黑名单
     * 
     * @param memberBlacklist 会员黑名单
     * @return 结果
     */
    @Override
    public int insertMemberBlacklist(MemberBlacklist memberBlacklist)
    {
        memberBlacklist.setCreateTime(DateUtils.getNowDate());
        return memberBlacklistMapper.insertMemberBlacklist(memberBlacklist);
    }

    /**
     * 修改会员黑名单
     * 
     * @param memberBlacklist 会员黑名单
     * @return 结果
     */
    @Override
    public int updateMemberBlacklist(MemberBlacklist memberBlacklist)
    {
        return memberBlacklistMapper.updateMemberBlacklist(memberBlacklist);
    }

    /**
     * 删除会员黑名单对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMemberBlacklistByIds(String ids)
    {
        return memberBlacklistMapper.deleteMemberBlacklistByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除会员黑名单信息
     * 
     * @param blackId 会员黑名单ID
     * @return 结果
     */
    @Override
    public int deleteMemberBlacklistById(Long blackId)
    {
        return memberBlacklistMapper.deleteMemberBlacklistById(blackId);
    }
}
