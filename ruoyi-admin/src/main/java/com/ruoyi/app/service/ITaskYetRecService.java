package com.ruoyi.app.service;

import com.ruoyi.app.domain.TaskYetRec;
import java.util.List;

/**
 * 任务记录Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface ITaskYetRecService 
{
    /**
     * 查询任务记录
     * 
     * @param yetId 任务记录ID
     * @return 任务记录
     */
    public TaskYetRec selectTaskYetRecById(String yetId);

    /**
     * 查询任务记录列表
     * 
     * @param taskYetRec 任务记录
     * @return 任务记录集合
     */
    public List<TaskYetRec> selectTaskYetRecList(TaskYetRec taskYetRec);

    /**
     * 新增任务记录
     * 
     * @param taskYetRec 任务记录
     * @return 结果
     */
    public int insertTaskYetRec(TaskYetRec taskYetRec);

    /**
     * 修改任务记录
     * 
     * @param taskYetRec 任务记录
     * @return 结果
     */
    public int updateTaskYetRec(TaskYetRec taskYetRec);

    /**
     * 批量删除任务记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTaskYetRecByIds(String ids);

    /**
     * 删除任务记录信息
     * 
     * @param yetId 任务记录ID
     * @return 结果
     */
    public int deleteTaskYetRecById(String yetId);
}
