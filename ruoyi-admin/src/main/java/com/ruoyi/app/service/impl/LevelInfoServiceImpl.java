package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.LevelInfoMapper;
import com.ruoyi.app.domain.LevelInfo;
import com.ruoyi.app.service.ILevelInfoService;
import com.ruoyi.common.core.text.Convert;

/**
 * 等级Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class LevelInfoServiceImpl implements ILevelInfoService 
{
    @Autowired
    private LevelInfoMapper levelInfoMapper;

    /**
     * 查询等级
     * 
     * @param levelId 等级ID
     * @return 等级
     */
    @Override
    public LevelInfo selectLevelInfoById(String levelId)
    {
        return levelInfoMapper.selectLevelInfoById(levelId);
    }

    /**
     * 查询等级列表
     * 
     * @param levelInfo 等级
     * @return 等级
     */
    @Override
    public List<LevelInfo> selectLevelInfoList(LevelInfo levelInfo)
    {
        return levelInfoMapper.selectLevelInfoList(levelInfo);
    }

    /**
     * 新增等级
     * 
     * @param levelInfo 等级
     * @return 结果
     */
    @Override
    public int insertLevelInfo(LevelInfo levelInfo)
    {
        levelInfo.setCreateTime(DateUtils.getNowDate());
        return levelInfoMapper.insertLevelInfo(levelInfo);
    }

    /**
     * 修改等级
     * 
     * @param levelInfo 等级
     * @return 结果
     */
    @Override
    public int updateLevelInfo(LevelInfo levelInfo)
    {
        return levelInfoMapper.updateLevelInfo(levelInfo);
    }

    /**
     * 删除等级对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteLevelInfoByIds(String ids)
    {
        return levelInfoMapper.deleteLevelInfoByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除等级信息
     * 
     * @param levelId 等级ID
     * @return 结果
     */
    @Override
    public int deleteLevelInfoById(String levelId)
    {
        return levelInfoMapper.deleteLevelInfoById(levelId);
    }
}
