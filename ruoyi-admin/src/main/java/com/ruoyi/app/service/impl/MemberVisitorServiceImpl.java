package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.MemberVisitorMapper;
import com.ruoyi.app.domain.MemberVisitor;
import com.ruoyi.app.service.IMemberVisitorService;
import com.ruoyi.common.core.text.Convert;

/**
 * 访客Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class MemberVisitorServiceImpl implements IMemberVisitorService 
{
    @Autowired
    private MemberVisitorMapper memberVisitorMapper;

    /**
     * 查询访客
     * 
     * @param visitorId 访客ID
     * @return 访客
     */
    @Override
    public MemberVisitor selectMemberVisitorById(Long visitorId)
    {
        return memberVisitorMapper.selectMemberVisitorById(visitorId);
    }

    /**
     * 查询访客列表
     * 
     * @param memberVisitor 访客
     * @return 访客
     */
    @Override
    public List<MemberVisitor> selectMemberVisitorList(MemberVisitor memberVisitor)
    {
        return memberVisitorMapper.selectMemberVisitorList(memberVisitor);
    }

    /**
     * 新增访客
     * 
     * @param memberVisitor 访客
     * @return 结果
     */
    @Override
    public int insertMemberVisitor(MemberVisitor memberVisitor)
    {
        memberVisitor.setCreateTime(DateUtils.getNowDate());
        return memberVisitorMapper.insertMemberVisitor(memberVisitor);
    }

    /**
     * 修改访客
     * 
     * @param memberVisitor 访客
     * @return 结果
     */
    @Override
    public int updateMemberVisitor(MemberVisitor memberVisitor)
    {
        return memberVisitorMapper.updateMemberVisitor(memberVisitor);
    }

    /**
     * 删除访客对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMemberVisitorByIds(String ids)
    {
        return memberVisitorMapper.deleteMemberVisitorByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除访客信息
     * 
     * @param visitorId 访客ID
     * @return 结果
     */
    @Override
    public int deleteMemberVisitorById(Long visitorId)
    {
        return memberVisitorMapper.deleteMemberVisitorById(visitorId);
    }
}
