package com.ruoyi.app.service;

import com.ruoyi.app.domain.NoteType;
import java.util.List;

/**
 * 游记类型Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface INoteTypeService 
{
    /**
     * 查询游记类型
     * 
     * @param noteTypeId 游记类型ID
     * @return 游记类型
     */
    public NoteType selectNoteTypeById(Long noteTypeId);

    /**
     * 查询游记类型列表
     * 
     * @param noteType 游记类型
     * @return 游记类型集合
     */
    public List<NoteType> selectNoteTypeList(NoteType noteType);

    /**
     * 新增游记类型
     * 
     * @param noteType 游记类型
     * @return 结果
     */
    public int insertNoteType(NoteType noteType);

    /**
     * 修改游记类型
     * 
     * @param noteType 游记类型
     * @return 结果
     */
    public int updateNoteType(NoteType noteType);

    /**
     * 批量删除游记类型
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteNoteTypeByIds(String ids);

    /**
     * 删除游记类型信息
     * 
     * @param noteTypeId 游记类型ID
     * @return 结果
     */
    public int deleteNoteTypeById(Long noteTypeId);
}
