package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.MemberBaseMapper;
import com.ruoyi.app.domain.MemberBase;
import com.ruoyi.app.service.IMemberBaseService;
import com.ruoyi.common.core.text.Convert;

/**
 * 会员基础Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-01
 */
@Service
public class MemberBaseServiceImpl implements IMemberBaseService 
{
    @Autowired
    private MemberBaseMapper memberBaseMapper;

    /**
     * 查询会员基础
     * 
     * @param memberId 会员基础ID
     * @return 会员基础
     */
    @Override
    public MemberBase selectMemberBaseById(String memberId)
    {
        return memberBaseMapper.selectMemberBaseById(memberId);
    }

    /**
     * 查询会员基础列表
     * 
     * @param memberBase 会员基础
     * @return 会员基础
     */
    @Override
    public List<MemberBase> selectMemberBaseList(MemberBase memberBase)
    {
        return memberBaseMapper.selectMemberBaseList(memberBase);
    }

    /**
     * 新增会员基础
     * 
     * @param memberBase 会员基础
     * @return 结果
     */
    @Override
    public int insertMemberBase(MemberBase memberBase)
    {
        memberBase.setCreateTime(DateUtils.getNowDate());
        return memberBaseMapper.insertMemberBase(memberBase);
    }

    /**
     * 修改会员基础
     * 
     * @param memberBase 会员基础
     * @return 结果
     */
    @Override
    public int updateMemberBase(MemberBase memberBase)
    {
        return memberBaseMapper.updateMemberBase(memberBase);
    }

    /**
     * 删除会员基础对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMemberBaseByIds(String ids)
    {
        return memberBaseMapper.deleteMemberBaseByIds(Convert.toStrArray(ids));
    }

    @Override
    public int updateHeadAuditNOByIds(String ids)
    {
        return memberBaseMapper.updateHeadAuditNOByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除会员基础信息
     * 
     * @param memberId 会员基础ID
     * @return 结果
     */
    @Override
    public int deleteMemberBaseById(String memberId)
    {
        return memberBaseMapper.deleteMemberBaseById(memberId);
    }
}
