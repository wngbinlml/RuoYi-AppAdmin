package com.ruoyi.app.service;

import com.ruoyi.app.domain.ShareIncome;
import java.util.List;

/**
 * 分享新用户收入Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface IShareIncomeService 
{
    /**
     * 查询分享新用户收入
     * 
     * @param shareIncomeId 分享新用户收入ID
     * @return 分享新用户收入
     */
    public ShareIncome selectShareIncomeById(String shareIncomeId);

    /**
     * 查询分享新用户收入列表
     * 
     * @param shareIncome 分享新用户收入
     * @return 分享新用户收入集合
     */
    public List<ShareIncome> selectShareIncomeList(ShareIncome shareIncome);

    /**
     * 新增分享新用户收入
     * 
     * @param shareIncome 分享新用户收入
     * @return 结果
     */
    public int insertShareIncome(ShareIncome shareIncome);

    /**
     * 修改分享新用户收入
     * 
     * @param shareIncome 分享新用户收入
     * @return 结果
     */
    public int updateShareIncome(ShareIncome shareIncome);

    /**
     * 批量删除分享新用户收入
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteShareIncomeByIds(String ids);

    /**
     * 删除分享新用户收入信息
     * 
     * @param shareIncomeId 分享新用户收入ID
     * @return 结果
     */
    public int deleteShareIncomeById(String shareIncomeId);
}
