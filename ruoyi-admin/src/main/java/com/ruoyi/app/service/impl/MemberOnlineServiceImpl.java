package com.ruoyi.app.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.MemberOnlineMapper;
import com.ruoyi.app.domain.MemberOnline;
import com.ruoyi.app.service.IMemberOnlineService;
import com.ruoyi.common.core.text.Convert;

/**
 * 会员在线Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class MemberOnlineServiceImpl implements IMemberOnlineService 
{
    @Autowired
    private MemberOnlineMapper memberOnlineMapper;

    /**
     * 查询会员在线
     * 
     * @param onlineId 会员在线ID
     * @return 会员在线
     */
    @Override
    public MemberOnline selectMemberOnlineById(Long onlineId)
    {
        return memberOnlineMapper.selectMemberOnlineById(onlineId);
    }

    /**
     * 查询会员在线列表
     * 
     * @param memberOnline 会员在线
     * @return 会员在线
     */
    @Override
    public List<MemberOnline> selectMemberOnlineList(MemberOnline memberOnline)
    {
        return memberOnlineMapper.selectMemberOnlineList(memberOnline);
    }

    /**
     * 新增会员在线
     * 
     * @param memberOnline 会员在线
     * @return 结果
     */
    @Override
    public int insertMemberOnline(MemberOnline memberOnline)
    {
        return memberOnlineMapper.insertMemberOnline(memberOnline);
    }

    /**
     * 修改会员在线
     * 
     * @param memberOnline 会员在线
     * @return 结果
     */
    @Override
    public int updateMemberOnline(MemberOnline memberOnline)
    {
        return memberOnlineMapper.updateMemberOnline(memberOnline);
    }

    /**
     * 删除会员在线对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMemberOnlineByIds(String ids)
    {
        return memberOnlineMapper.deleteMemberOnlineByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除会员在线信息
     * 
     * @param onlineId 会员在线ID
     * @return 结果
     */
    @Override
    public int deleteMemberOnlineById(Long onlineId)
    {
        return memberOnlineMapper.deleteMemberOnlineById(onlineId);
    }
}
