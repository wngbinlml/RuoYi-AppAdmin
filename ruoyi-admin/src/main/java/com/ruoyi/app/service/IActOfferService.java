package com.ruoyi.app.service;

import com.ruoyi.app.domain.ActOffer;
import java.util.List;

/**
 * 活动邀约Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface IActOfferService 
{
    /**
     * 查询活动邀约
     * 
     * @param offerId 活动邀约ID
     * @return 活动邀约
     */
    public ActOffer selectActOfferById(String offerId);

    /**
     * 查询活动邀约列表
     * 
     * @param actOffer 活动邀约
     * @return 活动邀约集合
     */
    public List<ActOffer> selectActOfferList(ActOffer actOffer);

    /**
     * 新增活动邀约
     * 
     * @param actOffer 活动邀约
     * @return 结果
     */
    public int insertActOffer(ActOffer actOffer);

    /**
     * 修改活动邀约
     * 
     * @param actOffer 活动邀约
     * @return 结果
     */
    public int updateActOffer(ActOffer actOffer);

    /**
     * 批量删除活动邀约
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteActOfferByIds(String ids);

    /**
     * 删除活动邀约信息
     * 
     * @param offerId 活动邀约ID
     * @return 结果
     */
    public int deleteActOfferById(String offerId);
}
