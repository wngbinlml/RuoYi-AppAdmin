package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.MemberDislikeMapper;
import com.ruoyi.app.domain.MemberDislike;
import com.ruoyi.app.service.IMemberDislikeService;
import com.ruoyi.common.core.text.Convert;

/**
 * 不喜欢Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class MemberDislikeServiceImpl implements IMemberDislikeService 
{
    @Autowired
    private MemberDislikeMapper memberDislikeMapper;

    /**
     * 查询不喜欢
     * 
     * @param dislikeId 不喜欢ID
     * @return 不喜欢
     */
    @Override
    public MemberDislike selectMemberDislikeById(Long dislikeId)
    {
        return memberDislikeMapper.selectMemberDislikeById(dislikeId);
    }

    /**
     * 查询不喜欢列表
     * 
     * @param memberDislike 不喜欢
     * @return 不喜欢
     */
    @Override
    public List<MemberDislike> selectMemberDislikeList(MemberDislike memberDislike)
    {
        return memberDislikeMapper.selectMemberDislikeList(memberDislike);
    }

    /**
     * 新增不喜欢
     * 
     * @param memberDislike 不喜欢
     * @return 结果
     */
    @Override
    public int insertMemberDislike(MemberDislike memberDislike)
    {
        memberDislike.setCreateTime(DateUtils.getNowDate());
        return memberDislikeMapper.insertMemberDislike(memberDislike);
    }

    /**
     * 修改不喜欢
     * 
     * @param memberDislike 不喜欢
     * @return 结果
     */
    @Override
    public int updateMemberDislike(MemberDislike memberDislike)
    {
        return memberDislikeMapper.updateMemberDislike(memberDislike);
    }

    /**
     * 删除不喜欢对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMemberDislikeByIds(String ids)
    {
        return memberDislikeMapper.deleteMemberDislikeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除不喜欢信息
     * 
     * @param dislikeId 不喜欢ID
     * @return 结果
     */
    @Override
    public int deleteMemberDislikeById(Long dislikeId)
    {
        return memberDislikeMapper.deleteMemberDislikeById(dislikeId);
    }
}
