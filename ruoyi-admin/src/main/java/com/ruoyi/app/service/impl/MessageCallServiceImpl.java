package com.ruoyi.app.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.MessageCallMapper;
import com.ruoyi.app.domain.MessageCall;
import com.ruoyi.app.service.IMessageCallService;
import com.ruoyi.common.core.text.Convert;

/**
 * 招呼Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class MessageCallServiceImpl implements IMessageCallService 
{
    @Autowired
    private MessageCallMapper messageCallMapper;

    /**
     * 查询招呼
     * 
     * @param callId 招呼ID
     * @return 招呼
     */
    @Override
    public MessageCall selectMessageCallById(String callId)
    {
        return messageCallMapper.selectMessageCallById(callId);
    }

    /**
     * 查询招呼列表
     * 
     * @param messageCall 招呼
     * @return 招呼
     */
    @Override
    public List<MessageCall> selectMessageCallList(MessageCall messageCall)
    {
        return messageCallMapper.selectMessageCallList(messageCall);
    }

    /**
     * 新增招呼
     * 
     * @param messageCall 招呼
     * @return 结果
     */
    @Override
    public int insertMessageCall(MessageCall messageCall)
    {
        return messageCallMapper.insertMessageCall(messageCall);
    }

    /**
     * 修改招呼
     * 
     * @param messageCall 招呼
     * @return 结果
     */
    @Override
    public int updateMessageCall(MessageCall messageCall)
    {
        return messageCallMapper.updateMessageCall(messageCall);
    }

    /**
     * 删除招呼对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMessageCallByIds(String ids)
    {
        return messageCallMapper.deleteMessageCallByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除招呼信息
     * 
     * @param callId 招呼ID
     * @return 结果
     */
    @Override
    public int deleteMessageCallById(String callId)
    {
        return messageCallMapper.deleteMessageCallById(callId);
    }
}
