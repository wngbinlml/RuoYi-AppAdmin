package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.GroupTypeMapper;
import com.ruoyi.app.domain.GroupType;
import com.ruoyi.app.service.IGroupTypeService;
import com.ruoyi.common.core.text.Convert;

/**
 * 群类型Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class GroupTypeServiceImpl implements IGroupTypeService 
{
    @Autowired
    private GroupTypeMapper groupTypeMapper;

    /**
     * 查询群类型
     * 
     * @param groupTypeId 群类型ID
     * @return 群类型
     */
    @Override
    public GroupType selectGroupTypeById(Long groupTypeId)
    {
        return groupTypeMapper.selectGroupTypeById(groupTypeId);
    }

    /**
     * 查询群类型列表
     * 
     * @param groupType 群类型
     * @return 群类型
     */
    @Override
    public List<GroupType> selectGroupTypeList(GroupType groupType)
    {
        return groupTypeMapper.selectGroupTypeList(groupType);
    }

    /**
     * 新增群类型
     * 
     * @param groupType 群类型
     * @return 结果
     */
    @Override
    public int insertGroupType(GroupType groupType)
    {
        groupType.setCreateTime(DateUtils.getNowDate());
        return groupTypeMapper.insertGroupType(groupType);
    }

    /**
     * 修改群类型
     * 
     * @param groupType 群类型
     * @return 结果
     */
    @Override
    public int updateGroupType(GroupType groupType)
    {
        return groupTypeMapper.updateGroupType(groupType);
    }

    /**
     * 删除群类型对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteGroupTypeByIds(String ids)
    {
        return groupTypeMapper.deleteGroupTypeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除群类型信息
     * 
     * @param groupTypeId 群类型ID
     * @return 结果
     */
    @Override
    public int deleteGroupTypeById(Long groupTypeId)
    {
        return groupTypeMapper.deleteGroupTypeById(groupTypeId);
    }
}
