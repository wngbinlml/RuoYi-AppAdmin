package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.CoinDealMapper;
import com.ruoyi.app.domain.CoinDeal;
import com.ruoyi.app.service.ICoinDealService;
import com.ruoyi.common.core.text.Convert;

/**
 * 趣币交易Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class CoinDealServiceImpl implements ICoinDealService 
{
    @Autowired
    private CoinDealMapper coinDealMapper;

    /**
     * 查询趣币交易
     * 
     * @param dealId 趣币交易ID
     * @return 趣币交易
     */
    @Override
    public CoinDeal selectCoinDealById(String dealId)
    {
        return coinDealMapper.selectCoinDealById(dealId);
    }

    /**
     * 查询趣币交易列表
     * 
     * @param coinDeal 趣币交易
     * @return 趣币交易
     */
    @Override
    public List<CoinDeal> selectCoinDealList(CoinDeal coinDeal)
    {
        return coinDealMapper.selectCoinDealList(coinDeal);
    }

    /**
     * 新增趣币交易
     * 
     * @param coinDeal 趣币交易
     * @return 结果
     */
    @Override
    public int insertCoinDeal(CoinDeal coinDeal)
    {
        coinDeal.setCreateTime(DateUtils.getNowDate());
        return coinDealMapper.insertCoinDeal(coinDeal);
    }

    /**
     * 修改趣币交易
     * 
     * @param coinDeal 趣币交易
     * @return 结果
     */
    @Override
    public int updateCoinDeal(CoinDeal coinDeal)
    {
        return coinDealMapper.updateCoinDeal(coinDeal);
    }

    /**
     * 删除趣币交易对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCoinDealByIds(String ids)
    {
        return coinDealMapper.deleteCoinDealByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除趣币交易信息
     * 
     * @param dealId 趣币交易ID
     * @return 结果
     */
    @Override
    public int deleteCoinDealById(String dealId)
    {
        return coinDealMapper.deleteCoinDealById(dealId);
    }
}
