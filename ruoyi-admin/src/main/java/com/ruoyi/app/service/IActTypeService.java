package com.ruoyi.app.service;

import com.ruoyi.app.domain.ActType;
import java.util.List;

/**
 * 活动类型Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface IActTypeService 
{
    /**
     * 查询活动类型
     * 
     * @param actTypeId 活动类型ID
     * @return 活动类型
     */
    public ActType selectActTypeById(Long actTypeId);

    /**
     * 查询活动类型列表
     * 
     * @param actType 活动类型
     * @return 活动类型集合
     */
    public List<ActType> selectActTypeList(ActType actType);

    /**
     * 新增活动类型
     * 
     * @param actType 活动类型
     * @return 结果
     */
    public int insertActType(ActType actType);

    /**
     * 修改活动类型
     * 
     * @param actType 活动类型
     * @return 结果
     */
    public int updateActType(ActType actType);

    /**
     * 批量删除活动类型
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteActTypeByIds(String ids);

    /**
     * 删除活动类型信息
     * 
     * @param actTypeId 活动类型ID
     * @return 结果
     */
    public int deleteActTypeById(Long actTypeId);
}
