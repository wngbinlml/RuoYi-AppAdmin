package com.ruoyi.app.service;

import com.ruoyi.app.domain.NotifyMsg;
import java.util.List;

/**
 * 通知Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface INotifyMsgService 
{
    /**
     * 查询通知
     * 
     * @param notifyId 通知ID
     * @return 通知
     */
    public NotifyMsg selectNotifyMsgById(String notifyId);

    /**
     * 查询通知列表
     * 
     * @param notifyMsg 通知
     * @return 通知集合
     */
    public List<NotifyMsg> selectNotifyMsgList(NotifyMsg notifyMsg);

    /**
     * 新增通知
     * 
     * @param notifyMsg 通知
     * @return 结果
     */
    public int insertNotifyMsg(NotifyMsg notifyMsg);

    /**
     * 修改通知
     * 
     * @param notifyMsg 通知
     * @return 结果
     */
    public int updateNotifyMsg(NotifyMsg notifyMsg);

    /**
     * 批量删除通知
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteNotifyMsgByIds(String ids);

    /**
     * 删除通知信息
     * 
     * @param notifyId 通知ID
     * @return 结果
     */
    public int deleteNotifyMsgById(String notifyId);
}
