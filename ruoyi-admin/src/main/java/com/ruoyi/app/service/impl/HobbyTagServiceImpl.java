package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.HobbyTagMapper;
import com.ruoyi.app.domain.HobbyTag;
import com.ruoyi.app.service.IHobbyTagService;
import com.ruoyi.common.core.text.Convert;

/**
 * 兴趣标签Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class HobbyTagServiceImpl implements IHobbyTagService 
{
    @Autowired
    private HobbyTagMapper hobbyTagMapper;

    /**
     * 查询兴趣标签
     * 
     * @param tagId 兴趣标签ID
     * @return 兴趣标签
     */
    @Override
    public HobbyTag selectHobbyTagById(String tagId)
    {
        return hobbyTagMapper.selectHobbyTagById(tagId);
    }

    /**
     * 查询兴趣标签列表
     * 
     * @param hobbyTag 兴趣标签
     * @return 兴趣标签
     */
    @Override
    public List<HobbyTag> selectHobbyTagList(HobbyTag hobbyTag)
    {
        return hobbyTagMapper.selectHobbyTagList(hobbyTag);
    }

    /**
     * 新增兴趣标签
     * 
     * @param hobbyTag 兴趣标签
     * @return 结果
     */
    @Override
    public int insertHobbyTag(HobbyTag hobbyTag)
    {
        hobbyTag.setCreateTime(DateUtils.getNowDate());
        return hobbyTagMapper.insertHobbyTag(hobbyTag);
    }

    /**
     * 修改兴趣标签
     * 
     * @param hobbyTag 兴趣标签
     * @return 结果
     */
    @Override
    public int updateHobbyTag(HobbyTag hobbyTag)
    {
        return hobbyTagMapper.updateHobbyTag(hobbyTag);
    }

    /**
     * 删除兴趣标签对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteHobbyTagByIds(String ids)
    {
        return hobbyTagMapper.deleteHobbyTagByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除兴趣标签信息
     * 
     * @param tagId 兴趣标签ID
     * @return 结果
     */
    @Override
    public int deleteHobbyTagById(String tagId)
    {
        return hobbyTagMapper.deleteHobbyTagById(tagId);
    }
}
