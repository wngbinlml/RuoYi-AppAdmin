package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.CollectMemberMapper;
import com.ruoyi.app.domain.CollectMember;
import com.ruoyi.app.service.ICollectMemberService;
import com.ruoyi.common.core.text.Convert;

/**
 * 收藏会员Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class CollectMemberServiceImpl implements ICollectMemberService 
{
    @Autowired
    private CollectMemberMapper collectMemberMapper;

    /**
     * 查询收藏会员
     * 
     * @param collectId 收藏会员ID
     * @return 收藏会员
     */
    @Override
    public CollectMember selectCollectMemberById(Long collectId)
    {
        return collectMemberMapper.selectCollectMemberById(collectId);
    }

    /**
     * 查询收藏会员列表
     * 
     * @param collectMember 收藏会员
     * @return 收藏会员
     */
    @Override
    public List<CollectMember> selectCollectMemberList(CollectMember collectMember)
    {
        return collectMemberMapper.selectCollectMemberList(collectMember);
    }

    /**
     * 新增收藏会员
     * 
     * @param collectMember 收藏会员
     * @return 结果
     */
    @Override
    public int insertCollectMember(CollectMember collectMember)
    {
        collectMember.setCreateTime(DateUtils.getNowDate());
        return collectMemberMapper.insertCollectMember(collectMember);
    }

    /**
     * 修改收藏会员
     * 
     * @param collectMember 收藏会员
     * @return 结果
     */
    @Override
    public int updateCollectMember(CollectMember collectMember)
    {
        return collectMemberMapper.updateCollectMember(collectMember);
    }

    /**
     * 删除收藏会员对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCollectMemberByIds(String ids)
    {
        return collectMemberMapper.deleteCollectMemberByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除收藏会员信息
     * 
     * @param collectId 收藏会员ID
     * @return 结果
     */
    @Override
    public int deleteCollectMemberById(Long collectId)
    {
        return collectMemberMapper.deleteCollectMemberById(collectId);
    }
}
