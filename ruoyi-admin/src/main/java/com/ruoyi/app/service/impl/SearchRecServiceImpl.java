package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.SearchRecMapper;
import com.ruoyi.app.domain.SearchRec;
import com.ruoyi.app.service.ISearchRecService;
import com.ruoyi.common.core.text.Convert;

/**
 * 搜索记录Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class SearchRecServiceImpl implements ISearchRecService 
{
    @Autowired
    private SearchRecMapper searchRecMapper;

    /**
     * 查询搜索记录
     * 
     * @param searchId 搜索记录ID
     * @return 搜索记录
     */
    @Override
    public SearchRec selectSearchRecById(Long searchId)
    {
        return searchRecMapper.selectSearchRecById(searchId);
    }

    /**
     * 查询搜索记录列表
     * 
     * @param searchRec 搜索记录
     * @return 搜索记录
     */
    @Override
    public List<SearchRec> selectSearchRecList(SearchRec searchRec)
    {
        return searchRecMapper.selectSearchRecList(searchRec);
    }

    /**
     * 新增搜索记录
     * 
     * @param searchRec 搜索记录
     * @return 结果
     */
    @Override
    public int insertSearchRec(SearchRec searchRec)
    {
        searchRec.setCreateTime(DateUtils.getNowDate());
        return searchRecMapper.insertSearchRec(searchRec);
    }

    /**
     * 修改搜索记录
     * 
     * @param searchRec 搜索记录
     * @return 结果
     */
    @Override
    public int updateSearchRec(SearchRec searchRec)
    {
        return searchRecMapper.updateSearchRec(searchRec);
    }

    /**
     * 删除搜索记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSearchRecByIds(String ids)
    {
        return searchRecMapper.deleteSearchRecByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除搜索记录信息
     * 
     * @param searchId 搜索记录ID
     * @return 结果
     */
    @Override
    public int deleteSearchRecById(Long searchId)
    {
        return searchRecMapper.deleteSearchRecById(searchId);
    }
}
