package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.TaskRuleMapper;
import com.ruoyi.app.domain.TaskRule;
import com.ruoyi.app.service.ITaskRuleService;
import com.ruoyi.common.core.text.Convert;

/**
 * 任务定义Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class TaskRuleServiceImpl implements ITaskRuleService 
{
    @Autowired
    private TaskRuleMapper taskRuleMapper;

    /**
     * 查询任务定义
     * 
     * @param taskId 任务定义ID
     * @return 任务定义
     */
    @Override
    public TaskRule selectTaskRuleById(String taskId)
    {
        return taskRuleMapper.selectTaskRuleById(taskId);
    }

    /**
     * 查询任务定义列表
     * 
     * @param taskRule 任务定义
     * @return 任务定义
     */
    @Override
    public List<TaskRule> selectTaskRuleList(TaskRule taskRule)
    {
        return taskRuleMapper.selectTaskRuleList(taskRule);
    }

    /**
     * 新增任务定义
     * 
     * @param taskRule 任务定义
     * @return 结果
     */
    @Override
    public int insertTaskRule(TaskRule taskRule)
    {
        taskRule.setCreateTime(DateUtils.getNowDate());
        return taskRuleMapper.insertTaskRule(taskRule);
    }

    /**
     * 修改任务定义
     * 
     * @param taskRule 任务定义
     * @return 结果
     */
    @Override
    public int updateTaskRule(TaskRule taskRule)
    {
        return taskRuleMapper.updateTaskRule(taskRule);
    }

    /**
     * 删除任务定义对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTaskRuleByIds(String ids)
    {
        return taskRuleMapper.deleteTaskRuleByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除任务定义信息
     * 
     * @param taskId 任务定义ID
     * @return 结果
     */
    @Override
    public int deleteTaskRuleById(String taskId)
    {
        return taskRuleMapper.deleteTaskRuleById(taskId);
    }
}
