package com.ruoyi.app.service;

import com.ruoyi.app.domain.HobbyTag;
import java.util.List;

/**
 * 兴趣标签Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface IHobbyTagService 
{
    /**
     * 查询兴趣标签
     * 
     * @param tagId 兴趣标签ID
     * @return 兴趣标签
     */
    public HobbyTag selectHobbyTagById(String tagId);

    /**
     * 查询兴趣标签列表
     * 
     * @param hobbyTag 兴趣标签
     * @return 兴趣标签集合
     */
    public List<HobbyTag> selectHobbyTagList(HobbyTag hobbyTag);

    /**
     * 新增兴趣标签
     * 
     * @param hobbyTag 兴趣标签
     * @return 结果
     */
    public int insertHobbyTag(HobbyTag hobbyTag);

    /**
     * 修改兴趣标签
     * 
     * @param hobbyTag 兴趣标签
     * @return 结果
     */
    public int updateHobbyTag(HobbyTag hobbyTag);

    /**
     * 批量删除兴趣标签
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteHobbyTagByIds(String ids);

    /**
     * 删除兴趣标签信息
     * 
     * @param tagId 兴趣标签ID
     * @return 结果
     */
    public int deleteHobbyTagById(String tagId);
}
