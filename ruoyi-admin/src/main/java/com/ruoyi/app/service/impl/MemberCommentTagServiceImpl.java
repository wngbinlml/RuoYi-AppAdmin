package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.MemberCommentTagMapper;
import com.ruoyi.app.domain.MemberCommentTag;
import com.ruoyi.app.service.IMemberCommentTagService;
import com.ruoyi.common.core.text.Convert;

/**
 * 会员评价标签Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class MemberCommentTagServiceImpl implements IMemberCommentTagService 
{
    @Autowired
    private MemberCommentTagMapper memberCommentTagMapper;

    /**
     * 查询会员评价标签
     * 
     * @param tagId 会员评价标签ID
     * @return 会员评价标签
     */
    @Override
    public MemberCommentTag selectMemberCommentTagById(Long tagId)
    {
        return memberCommentTagMapper.selectMemberCommentTagById(tagId);
    }

    /**
     * 查询会员评价标签列表
     * 
     * @param memberCommentTag 会员评价标签
     * @return 会员评价标签
     */
    @Override
    public List<MemberCommentTag> selectMemberCommentTagList(MemberCommentTag memberCommentTag)
    {
        return memberCommentTagMapper.selectMemberCommentTagList(memberCommentTag);
    }

    /**
     * 新增会员评价标签
     * 
     * @param memberCommentTag 会员评价标签
     * @return 结果
     */
    @Override
    public int insertMemberCommentTag(MemberCommentTag memberCommentTag)
    {
        memberCommentTag.setCreateTime(DateUtils.getNowDate());
        return memberCommentTagMapper.insertMemberCommentTag(memberCommentTag);
    }

    /**
     * 修改会员评价标签
     * 
     * @param memberCommentTag 会员评价标签
     * @return 结果
     */
    @Override
    public int updateMemberCommentTag(MemberCommentTag memberCommentTag)
    {
        return memberCommentTagMapper.updateMemberCommentTag(memberCommentTag);
    }

    /**
     * 删除会员评价标签对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMemberCommentTagByIds(String ids)
    {
        return memberCommentTagMapper.deleteMemberCommentTagByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除会员评价标签信息
     * 
     * @param tagId 会员评价标签ID
     * @return 结果
     */
    @Override
    public int deleteMemberCommentTagById(Long tagId)
    {
        return memberCommentTagMapper.deleteMemberCommentTagById(tagId);
    }
}
