package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.MessageCall;
import com.ruoyi.app.service.IMessageCallService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 招呼Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/messageCall")
public class MessageCallController extends BaseController
{
    private String prefix = "app/messageCall";

    @Autowired
    private IMessageCallService messageCallService;

    @RequiresPermissions("app:messageCall:view")
    @GetMapping()
    public String messageCall()
    {
        return prefix + "/messageCall";
    }

    /**
     * 查询招呼列表
     */
    @RequiresPermissions("app:messageCall:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MessageCall messageCall)
    {
        startPage();
        List<MessageCall> list = messageCallService.selectMessageCallList(messageCall);
        return getDataTable(list);
    }

    /**
     * 导出招呼列表
     */
    @RequiresPermissions("app:messageCall:export")
    @Log(title = "招呼", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MessageCall messageCall)
    {
        List<MessageCall> list = messageCallService.selectMessageCallList(messageCall);
        ExcelUtil<MessageCall> util = new ExcelUtil<MessageCall>(MessageCall.class);
        return util.exportExcel(list, "messageCall");
    }

    /**
     * 新增招呼
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存招呼
     */
    @RequiresPermissions("app:messageCall:add")
    @Log(title = "招呼", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MessageCall messageCall)
    {
        return toAjax(messageCallService.insertMessageCall(messageCall));
    }

    /**
     * 修改招呼
     */
    @GetMapping("/edit/{callId}")
    public String edit(@PathVariable("callId") String callId, ModelMap mmap)
    {
        MessageCall messageCall = messageCallService.selectMessageCallById(callId);
        mmap.put("messageCall", messageCall);
        return prefix + "/edit";
    }

    /**
     * 修改保存招呼
     */
    @RequiresPermissions("app:messageCall:edit")
    @Log(title = "招呼", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MessageCall messageCall)
    {
        return toAjax(messageCallService.updateMessageCall(messageCall));
    }

    /**
     * 删除招呼
     */
    @RequiresPermissions("app:messageCall:remove")
    @Log(title = "招呼", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(messageCallService.deleteMessageCallByIds(ids));
    }
}
