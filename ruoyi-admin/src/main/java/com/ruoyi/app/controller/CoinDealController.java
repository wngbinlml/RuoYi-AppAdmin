package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.CoinDeal;
import com.ruoyi.app.service.ICoinDealService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 趣币交易Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/coinDeal")
public class CoinDealController extends BaseController
{
    private String prefix = "app/coinDeal";

    @Autowired
    private ICoinDealService coinDealService;

    @RequiresPermissions("app:coinDeal:view")
    @GetMapping()
    public String coinDeal()
    {
        return prefix + "/coinDeal";
    }

    /**
     * 查询趣币交易列表
     */
    @RequiresPermissions("app:coinDeal:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CoinDeal coinDeal)
    {
        startPage();
        List<CoinDeal> list = coinDealService.selectCoinDealList(coinDeal);
        return getDataTable(list);
    }

    /**
     * 导出趣币交易列表
     */
    @RequiresPermissions("app:coinDeal:export")
    @Log(title = "趣币交易", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CoinDeal coinDeal)
    {
        List<CoinDeal> list = coinDealService.selectCoinDealList(coinDeal);
        ExcelUtil<CoinDeal> util = new ExcelUtil<CoinDeal>(CoinDeal.class);
        return util.exportExcel(list, "coinDeal");
    }

    /**
     * 新增趣币交易
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存趣币交易
     */
    @RequiresPermissions("app:coinDeal:add")
    @Log(title = "趣币交易", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CoinDeal coinDeal)
    {
        return toAjax(coinDealService.insertCoinDeal(coinDeal));
    }

    /**
     * 修改趣币交易
     */
    @GetMapping("/edit/{dealId}")
    public String edit(@PathVariable("dealId") String dealId, ModelMap mmap)
    {
        CoinDeal coinDeal = coinDealService.selectCoinDealById(dealId);
        mmap.put("coinDeal", coinDeal);
        return prefix + "/edit";
    }

    /**
     * 修改保存趣币交易
     */
    @RequiresPermissions("app:coinDeal:edit")
    @Log(title = "趣币交易", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CoinDeal coinDeal)
    {
        return toAjax(coinDealService.updateCoinDeal(coinDeal));
    }

    /**
     * 删除趣币交易
     */
    @RequiresPermissions("app:coinDeal:remove")
    @Log(title = "趣币交易", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(coinDealService.deleteCoinDealByIds(ids));
    }
}
