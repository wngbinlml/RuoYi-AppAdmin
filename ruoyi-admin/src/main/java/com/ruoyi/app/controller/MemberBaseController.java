package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.MemberBase;
import com.ruoyi.app.service.IMemberBaseService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 会员基础Controller
 * 
 * @author bigStream
 * @date 2020-08-01
 */
@Controller
@RequestMapping("/app/memberBase")
public class MemberBaseController extends BaseController
{
    private String prefix = "app/memberBase";

    @Autowired
    private IMemberBaseService memberBaseService;

    @RequiresPermissions("app:memberBase:view")
    @GetMapping()
    public String memberBase()
    {
        return prefix + "/memberBase";
    }

    /**
     * 查询会员基础列表
     */
    @RequiresPermissions("app:memberBase:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MemberBase memberBase)
    {
        startPage();
        List<MemberBase> list = memberBaseService.selectMemberBaseList(memberBase);
        return getDataTable(list);
    }

    /**
     * 导出会员基础列表
     */
    @RequiresPermissions("app:memberBase:export")
    @Log(title = "会员基础", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MemberBase memberBase)
    {
        List<MemberBase> list = memberBaseService.selectMemberBaseList(memberBase);
        ExcelUtil<MemberBase> util = new ExcelUtil<MemberBase>(MemberBase.class);
        return util.exportExcel(list, "memberBase");
    }

    /**
     * 新增会员基础
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存会员基础
     */
    @RequiresPermissions("app:memberBase:add")
    @Log(title = "会员基础", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MemberBase memberBase)
    {
        return toAjax(memberBaseService.insertMemberBase(memberBase));
    }

    /**
     * 修改会员基础
     */
    @GetMapping("/edit/{memberId}")
    public String edit(@PathVariable("memberId") String memberId, ModelMap mmap)
    {
        MemberBase memberBase = memberBaseService.selectMemberBaseById(memberId);
        mmap.put("memberBase", memberBase);
        return prefix + "/edit";
    }

    /**
     * 修改保存会员基础
     */
    @RequiresPermissions("app:memberBase:edit")
    @Log(title = "会员基础", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MemberBase memberBase)
    {
        return toAjax(memberBaseService.updateMemberBase(memberBase));
    }

    /**
     * 删除会员基础
     */
    @RequiresPermissions("app:memberBase:remove")
    @Log(title = "会员基础", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(memberBaseService.deleteMemberBaseByIds(ids));
    }

    /**
     * 审核头像
     */
    @RequiresPermissions("app:memberBase:remove")
    @Log(title = "会员基础", businessType = BusinessType.DELETE)
    @PostMapping("/batchPassQc")
    @ResponseBody
    public AjaxResult batchPassQc(String ids) {
        return toAjax(memberBaseService.updateHeadAuditNOByIds(ids));
    }
}
