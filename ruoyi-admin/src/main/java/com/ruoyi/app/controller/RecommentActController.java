package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.RecommentAct;
import com.ruoyi.app.service.IRecommentActService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 推荐活动Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/recommentAct")
public class RecommentActController extends BaseController
{
    private String prefix = "app/recommentAct";

    @Autowired
    private IRecommentActService recommentActService;

    @RequiresPermissions("app:recommentAct:view")
    @GetMapping()
    public String recommentAct()
    {
        return prefix + "/recommentAct";
    }

    /**
     * 查询推荐活动列表
     */
    @RequiresPermissions("app:recommentAct:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(RecommentAct recommentAct)
    {
        startPage();
        List<RecommentAct> list = recommentActService.selectRecommentActList(recommentAct);
        return getDataTable(list);
    }

    /**
     * 导出推荐活动列表
     */
    @RequiresPermissions("app:recommentAct:export")
    @Log(title = "推荐活动", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(RecommentAct recommentAct)
    {
        List<RecommentAct> list = recommentActService.selectRecommentActList(recommentAct);
        ExcelUtil<RecommentAct> util = new ExcelUtil<RecommentAct>(RecommentAct.class);
        return util.exportExcel(list, "recommentAct");
    }

    /**
     * 新增推荐活动
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存推荐活动
     */
    @RequiresPermissions("app:recommentAct:add")
    @Log(title = "推荐活动", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(RecommentAct recommentAct)
    {
        return toAjax(recommentActService.insertRecommentAct(recommentAct));
    }

    /**
     * 修改推荐活动
     */
    @GetMapping("/edit/{recId}")
    public String edit(@PathVariable("recId") String recId, ModelMap mmap)
    {
        RecommentAct recommentAct = recommentActService.selectRecommentActById(recId);
        mmap.put("recommentAct", recommentAct);
        return prefix + "/edit";
    }

    /**
     * 修改保存推荐活动
     */
    @RequiresPermissions("app:recommentAct:edit")
    @Log(title = "推荐活动", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(RecommentAct recommentAct)
    {
        return toAjax(recommentActService.updateRecommentAct(recommentAct));
    }

    /**
     * 删除推荐活动
     */
    @RequiresPermissions("app:recommentAct:remove")
    @Log(title = "推荐活动", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(recommentActService.deleteRecommentActByIds(ids));
    }
}
