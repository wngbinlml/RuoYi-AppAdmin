package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.MemberBlacklist;
import com.ruoyi.app.service.IMemberBlacklistService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 会员黑名单Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/memberBlacklist")
public class MemberBlacklistController extends BaseController
{
    private String prefix = "app/memberBlacklist";

    @Autowired
    private IMemberBlacklistService memberBlacklistService;

    @RequiresPermissions("app:memberBlacklist:view")
    @GetMapping()
    public String memberBlacklist()
    {
        return prefix + "/memberBlacklist";
    }

    /**
     * 查询会员黑名单列表
     */
    @RequiresPermissions("app:memberBlacklist:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MemberBlacklist memberBlacklist)
    {
        startPage();
        List<MemberBlacklist> list = memberBlacklistService.selectMemberBlacklistList(memberBlacklist);
        return getDataTable(list);
    }

    /**
     * 导出会员黑名单列表
     */
    @RequiresPermissions("app:memberBlacklist:export")
    @Log(title = "会员黑名单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MemberBlacklist memberBlacklist)
    {
        List<MemberBlacklist> list = memberBlacklistService.selectMemberBlacklistList(memberBlacklist);
        ExcelUtil<MemberBlacklist> util = new ExcelUtil<MemberBlacklist>(MemberBlacklist.class);
        return util.exportExcel(list, "memberBlacklist");
    }

    /**
     * 新增会员黑名单
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存会员黑名单
     */
    @RequiresPermissions("app:memberBlacklist:add")
    @Log(title = "会员黑名单", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MemberBlacklist memberBlacklist)
    {
        return toAjax(memberBlacklistService.insertMemberBlacklist(memberBlacklist));
    }

    /**
     * 修改会员黑名单
     */
    @GetMapping("/edit/{blackId}")
    public String edit(@PathVariable("blackId") Long blackId, ModelMap mmap)
    {
        MemberBlacklist memberBlacklist = memberBlacklistService.selectMemberBlacklistById(blackId);
        mmap.put("memberBlacklist", memberBlacklist);
        return prefix + "/edit";
    }

    /**
     * 修改保存会员黑名单
     */
    @RequiresPermissions("app:memberBlacklist:edit")
    @Log(title = "会员黑名单", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MemberBlacklist memberBlacklist)
    {
        return toAjax(memberBlacklistService.updateMemberBlacklist(memberBlacklist));
    }

    /**
     * 删除会员黑名单
     */
    @RequiresPermissions("app:memberBlacklist:remove")
    @Log(title = "会员黑名单", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(memberBlacklistService.deleteMemberBlacklistByIds(ids));
    }
}
