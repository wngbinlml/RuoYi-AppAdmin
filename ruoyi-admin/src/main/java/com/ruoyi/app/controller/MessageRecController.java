package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.MessageRec;
import com.ruoyi.app.service.IMessageRecService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 消息记录Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/messageRec")
public class MessageRecController extends BaseController
{
    private String prefix = "app/messageRec";

    @Autowired
    private IMessageRecService messageRecService;

    @RequiresPermissions("app:messageRec:view")
    @GetMapping()
    public String messageRec()
    {
        return prefix + "/messageRec";
    }

    /**
     * 查询消息记录列表
     */
    @RequiresPermissions("app:messageRec:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MessageRec messageRec)
    {
        startPage();
        List<MessageRec> list = messageRecService.selectMessageRecList(messageRec);
        return getDataTable(list);
    }

    /**
     * 导出消息记录列表
     */
    @RequiresPermissions("app:messageRec:export")
    @Log(title = "消息记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MessageRec messageRec)
    {
        List<MessageRec> list = messageRecService.selectMessageRecList(messageRec);
        ExcelUtil<MessageRec> util = new ExcelUtil<MessageRec>(MessageRec.class);
        return util.exportExcel(list, "messageRec");
    }

    /**
     * 新增消息记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存消息记录
     */
    @RequiresPermissions("app:messageRec:add")
    @Log(title = "消息记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MessageRec messageRec)
    {
        return toAjax(messageRecService.insertMessageRec(messageRec));
    }

    /**
     * 修改消息记录
     */
    @GetMapping("/edit/{msgId}")
    public String edit(@PathVariable("msgId") String msgId, ModelMap mmap)
    {
        MessageRec messageRec = messageRecService.selectMessageRecById(msgId);
        mmap.put("messageRec", messageRec);
        return prefix + "/edit";
    }

    /**
     * 修改保存消息记录
     */
    @RequiresPermissions("app:messageRec:edit")
    @Log(title = "消息记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MessageRec messageRec)
    {
        return toAjax(messageRecService.updateMessageRec(messageRec));
    }

    /**
     * 删除消息记录
     */
    @RequiresPermissions("app:messageRec:remove")
    @Log(title = "消息记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(messageRecService.deleteMessageRecByIds(ids));
    }
}
