package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.PkRec;
import com.ruoyi.app.service.IPkRecService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * pk比赛Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/pkRec")
public class PkRecController extends BaseController
{
    private String prefix = "app/pkRec";

    @Autowired
    private IPkRecService pkRecService;

    @RequiresPermissions("app:pkRec:view")
    @GetMapping()
    public String pkRec()
    {
        return prefix + "/pkRec";
    }

    /**
     * 查询pk比赛列表
     */
    @RequiresPermissions("app:pkRec:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PkRec pkRec)
    {
        startPage();
        List<PkRec> list = pkRecService.selectPkRecList(pkRec);
        return getDataTable(list);
    }

    /**
     * 导出pk比赛列表
     */
    @RequiresPermissions("app:pkRec:export")
    @Log(title = "pk比赛", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PkRec pkRec)
    {
        List<PkRec> list = pkRecService.selectPkRecList(pkRec);
        ExcelUtil<PkRec> util = new ExcelUtil<PkRec>(PkRec.class);
        return util.exportExcel(list, "pkRec");
    }

    /**
     * 新增pk比赛
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存pk比赛
     */
    @RequiresPermissions("app:pkRec:add")
    @Log(title = "pk比赛", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PkRec pkRec)
    {
        return toAjax(pkRecService.insertPkRec(pkRec));
    }

    /**
     * 修改pk比赛
     */
    @GetMapping("/edit/{pkId}")
    public String edit(@PathVariable("pkId") String pkId, ModelMap mmap)
    {
        PkRec pkRec = pkRecService.selectPkRecById(pkId);
        mmap.put("pkRec", pkRec);
        return prefix + "/edit";
    }

    /**
     * 修改保存pk比赛
     */
    @RequiresPermissions("app:pkRec:edit")
    @Log(title = "pk比赛", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PkRec pkRec)
    {
        return toAjax(pkRecService.updatePkRec(pkRec));
    }

    /**
     * 删除pk比赛
     */
    @RequiresPermissions("app:pkRec:remove")
    @Log(title = "pk比赛", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(pkRecService.deletePkRecByIds(ids));
    }
}
