package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.PlazaComment;
import com.ruoyi.app.service.IPlazaCommentService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 广场评论Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/plazaComment")
public class PlazaCommentController extends BaseController
{
    private String prefix = "app/plazaComment";

    @Autowired
    private IPlazaCommentService plazaCommentService;

    @RequiresPermissions("app:plazaComment:view")
    @GetMapping()
    public String plazaComment()
    {
        return prefix + "/plazaComment";
    }

    /**
     * 查询广场评论列表
     */
    @RequiresPermissions("app:plazaComment:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PlazaComment plazaComment)
    {
        startPage();
        List<PlazaComment> list = plazaCommentService.selectPlazaCommentList(plazaComment);
        return getDataTable(list);
    }

    /**
     * 导出广场评论列表
     */
    @RequiresPermissions("app:plazaComment:export")
    @Log(title = "广场评论", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PlazaComment plazaComment)
    {
        List<PlazaComment> list = plazaCommentService.selectPlazaCommentList(plazaComment);
        ExcelUtil<PlazaComment> util = new ExcelUtil<PlazaComment>(PlazaComment.class);
        return util.exportExcel(list, "plazaComment");
    }

    /**
     * 新增广场评论
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存广场评论
     */
    @RequiresPermissions("app:plazaComment:add")
    @Log(title = "广场评论", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PlazaComment plazaComment)
    {
        return toAjax(plazaCommentService.insertPlazaComment(plazaComment));
    }

    /**
     * 修改广场评论
     */
    @GetMapping("/edit/{plazaCommId}")
    public String edit(@PathVariable("plazaCommId") String plazaCommId, ModelMap mmap)
    {
        PlazaComment plazaComment = plazaCommentService.selectPlazaCommentById(plazaCommId);
        mmap.put("plazaComment", plazaComment);
        return prefix + "/edit";
    }

    /**
     * 修改保存广场评论
     */
    @RequiresPermissions("app:plazaComment:edit")
    @Log(title = "广场评论", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PlazaComment plazaComment)
    {
        return toAjax(plazaCommentService.updatePlazaComment(plazaComment));
    }

    /**
     * 删除广场评论
     */
    @RequiresPermissions("app:plazaComment:remove")
    @Log(title = "广场评论", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(plazaCommentService.deletePlazaCommentByIds(ids));
    }
}
