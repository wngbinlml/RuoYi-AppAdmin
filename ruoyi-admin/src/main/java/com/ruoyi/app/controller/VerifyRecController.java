package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.VerifyRec;
import com.ruoyi.app.service.IVerifyRecService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 验证码记录Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/verifyRec")
public class VerifyRecController extends BaseController
{
    private String prefix = "app/verifyRec";

    @Autowired
    private IVerifyRecService verifyRecService;

    @RequiresPermissions("app:verifyRec:view")
    @GetMapping()
    public String verifyRec()
    {
        return prefix + "/verifyRec";
    }

    /**
     * 查询验证码记录列表
     */
    @RequiresPermissions("app:verifyRec:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(VerifyRec verifyRec)
    {
        startPage();
        List<VerifyRec> list = verifyRecService.selectVerifyRecList(verifyRec);
        return getDataTable(list);
    }

    /**
     * 导出验证码记录列表
     */
    @RequiresPermissions("app:verifyRec:export")
    @Log(title = "验证码记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(VerifyRec verifyRec)
    {
        List<VerifyRec> list = verifyRecService.selectVerifyRecList(verifyRec);
        ExcelUtil<VerifyRec> util = new ExcelUtil<VerifyRec>(VerifyRec.class);
        return util.exportExcel(list, "verifyRec");
    }

    /**
     * 新增验证码记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存验证码记录
     */
    @RequiresPermissions("app:verifyRec:add")
    @Log(title = "验证码记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(VerifyRec verifyRec)
    {
        return toAjax(verifyRecService.insertVerifyRec(verifyRec));
    }

    /**
     * 修改验证码记录
     */
    @GetMapping("/edit/{verifyId}")
    public String edit(@PathVariable("verifyId") String verifyId, ModelMap mmap)
    {
        VerifyRec verifyRec = verifyRecService.selectVerifyRecById(verifyId);
        mmap.put("verifyRec", verifyRec);
        return prefix + "/edit";
    }

    /**
     * 修改保存验证码记录
     */
    @RequiresPermissions("app:verifyRec:edit")
    @Log(title = "验证码记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(VerifyRec verifyRec)
    {
        return toAjax(verifyRecService.updateVerifyRec(verifyRec));
    }

    /**
     * 删除验证码记录
     */
    @RequiresPermissions("app:verifyRec:remove")
    @Log(title = "验证码记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(verifyRecService.deleteVerifyRecByIds(ids));
    }
}
