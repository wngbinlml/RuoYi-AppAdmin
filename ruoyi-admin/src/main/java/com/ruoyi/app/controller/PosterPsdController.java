package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.PosterPsd;
import com.ruoyi.app.service.IPosterPsdService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 举报Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/posterPsd")
public class PosterPsdController extends BaseController
{
    private String prefix = "app/posterPsd";

    @Autowired
    private IPosterPsdService posterPsdService;

    @RequiresPermissions("app:posterPsd:view")
    @GetMapping()
    public String posterPsd()
    {
        return prefix + "/posterPsd";
    }

    /**
     * 查询举报列表
     */
    @RequiresPermissions("app:posterPsd:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PosterPsd posterPsd)
    {
        startPage();
        List<PosterPsd> list = posterPsdService.selectPosterPsdList(posterPsd);
        return getDataTable(list);
    }

    /**
     * 导出举报列表
     */
    @RequiresPermissions("app:posterPsd:export")
    @Log(title = "举报", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PosterPsd posterPsd)
    {
        List<PosterPsd> list = posterPsdService.selectPosterPsdList(posterPsd);
        ExcelUtil<PosterPsd> util = new ExcelUtil<PosterPsd>(PosterPsd.class);
        return util.exportExcel(list, "posterPsd");
    }

    /**
     * 新增举报
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存举报
     */
    @RequiresPermissions("app:posterPsd:add")
    @Log(title = "举报", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PosterPsd posterPsd)
    {
        return toAjax(posterPsdService.insertPosterPsd(posterPsd));
    }

    /**
     * 修改举报
     */
    @GetMapping("/edit/{psdId}")
    public String edit(@PathVariable("psdId") Long psdId, ModelMap mmap)
    {
        PosterPsd posterPsd = posterPsdService.selectPosterPsdById(psdId);
        mmap.put("posterPsd", posterPsd);
        return prefix + "/edit";
    }

    /**
     * 修改保存举报
     */
    @RequiresPermissions("app:posterPsd:edit")
    @Log(title = "举报", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PosterPsd posterPsd)
    {
        return toAjax(posterPsdService.updatePosterPsd(posterPsd));
    }

    /**
     * 删除举报
     */
    @RequiresPermissions("app:posterPsd:remove")
    @Log(title = "举报", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(posterPsdService.deletePosterPsdByIds(ids));
    }
}
