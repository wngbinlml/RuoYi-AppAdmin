package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.SearchRec;
import com.ruoyi.app.service.ISearchRecService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 搜索记录Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/searchRec")
public class SearchRecController extends BaseController
{
    private String prefix = "app/searchRec";

    @Autowired
    private ISearchRecService searchRecService;

    @RequiresPermissions("app:searchRec:view")
    @GetMapping()
    public String searchRec()
    {
        return prefix + "/searchRec";
    }

    /**
     * 查询搜索记录列表
     */
    @RequiresPermissions("app:searchRec:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SearchRec searchRec)
    {
        startPage();
        List<SearchRec> list = searchRecService.selectSearchRecList(searchRec);
        return getDataTable(list);
    }

    /**
     * 导出搜索记录列表
     */
    @RequiresPermissions("app:searchRec:export")
    @Log(title = "搜索记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SearchRec searchRec)
    {
        List<SearchRec> list = searchRecService.selectSearchRecList(searchRec);
        ExcelUtil<SearchRec> util = new ExcelUtil<SearchRec>(SearchRec.class);
        return util.exportExcel(list, "searchRec");
    }

    /**
     * 新增搜索记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存搜索记录
     */
    @RequiresPermissions("app:searchRec:add")
    @Log(title = "搜索记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SearchRec searchRec)
    {
        return toAjax(searchRecService.insertSearchRec(searchRec));
    }

    /**
     * 修改搜索记录
     */
    @GetMapping("/edit/{searchId}")
    public String edit(@PathVariable("searchId") Long searchId, ModelMap mmap)
    {
        SearchRec searchRec = searchRecService.selectSearchRecById(searchId);
        mmap.put("searchRec", searchRec);
        return prefix + "/edit";
    }

    /**
     * 修改保存搜索记录
     */
    @RequiresPermissions("app:searchRec:edit")
    @Log(title = "搜索记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SearchRec searchRec)
    {
        return toAjax(searchRecService.updateSearchRec(searchRec));
    }

    /**
     * 删除搜索记录
     */
    @RequiresPermissions("app:searchRec:remove")
    @Log(title = "搜索记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(searchRecService.deleteSearchRecByIds(ids));
    }
}
