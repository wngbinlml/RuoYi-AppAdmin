package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.MemberOnline;
import com.ruoyi.app.service.IMemberOnlineService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 会员在线Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/memberOnline")
public class MemberOnlineController extends BaseController
{
    private String prefix = "app/memberOnline";

    @Autowired
    private IMemberOnlineService memberOnlineService;

    @RequiresPermissions("app:memberOnline:view")
    @GetMapping()
    public String memberOnline()
    {
        return prefix + "/memberOnline";
    }

    /**
     * 查询会员在线列表
     */
    @RequiresPermissions("app:memberOnline:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MemberOnline memberOnline)
    {
        startPage();
        List<MemberOnline> list = memberOnlineService.selectMemberOnlineList(memberOnline);
        return getDataTable(list);
    }

    /**
     * 导出会员在线列表
     */
    @RequiresPermissions("app:memberOnline:export")
    @Log(title = "会员在线", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MemberOnline memberOnline)
    {
        List<MemberOnline> list = memberOnlineService.selectMemberOnlineList(memberOnline);
        ExcelUtil<MemberOnline> util = new ExcelUtil<MemberOnline>(MemberOnline.class);
        return util.exportExcel(list, "memberOnline");
    }

    /**
     * 新增会员在线
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存会员在线
     */
    @RequiresPermissions("app:memberOnline:add")
    @Log(title = "会员在线", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MemberOnline memberOnline)
    {
        return toAjax(memberOnlineService.insertMemberOnline(memberOnline));
    }

    /**
     * 修改会员在线
     */
    @GetMapping("/edit/{onlineId}")
    public String edit(@PathVariable("onlineId") Long onlineId, ModelMap mmap)
    {
        MemberOnline memberOnline = memberOnlineService.selectMemberOnlineById(onlineId);
        mmap.put("memberOnline", memberOnline);
        return prefix + "/edit";
    }

    /**
     * 修改保存会员在线
     */
    @RequiresPermissions("app:memberOnline:edit")
    @Log(title = "会员在线", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MemberOnline memberOnline)
    {
        return toAjax(memberOnlineService.updateMemberOnline(memberOnline));
    }

    /**
     * 删除会员在线
     */
    @RequiresPermissions("app:memberOnline:remove")
    @Log(title = "会员在线", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(memberOnlineService.deleteMemberOnlineByIds(ids));
    }
}
