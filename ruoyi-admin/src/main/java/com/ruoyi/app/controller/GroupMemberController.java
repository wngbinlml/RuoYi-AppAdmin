package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.GroupMember;
import com.ruoyi.app.service.IGroupMemberService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 加群记录Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/groupMember")
public class GroupMemberController extends BaseController
{
    private String prefix = "app/groupMember";

    @Autowired
    private IGroupMemberService groupMemberService;

    @RequiresPermissions("app:groupMember:view")
    @GetMapping()
    public String groupMember()
    {
        return prefix + "/groupMember";
    }

    /**
     * 查询加群记录列表
     */
    @RequiresPermissions("app:groupMember:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(GroupMember groupMember)
    {
        startPage();
        List<GroupMember> list = groupMemberService.selectGroupMemberList(groupMember);
        return getDataTable(list);
    }

    /**
     * 导出加群记录列表
     */
    @RequiresPermissions("app:groupMember:export")
    @Log(title = "加群记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(GroupMember groupMember)
    {
        List<GroupMember> list = groupMemberService.selectGroupMemberList(groupMember);
        ExcelUtil<GroupMember> util = new ExcelUtil<GroupMember>(GroupMember.class);
        return util.exportExcel(list, "groupMember");
    }

    /**
     * 新增加群记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存加群记录
     */
    @RequiresPermissions("app:groupMember:add")
    @Log(title = "加群记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(GroupMember groupMember)
    {
        return toAjax(groupMemberService.insertGroupMember(groupMember));
    }

    /**
     * 修改加群记录
     */
    @GetMapping("/edit/{agmId}")
    public String edit(@PathVariable("agmId") String agmId, ModelMap mmap)
    {
        GroupMember groupMember = groupMemberService.selectGroupMemberById(agmId);
        mmap.put("groupMember", groupMember);
        return prefix + "/edit";
    }

    /**
     * 修改保存加群记录
     */
    @RequiresPermissions("app:groupMember:edit")
    @Log(title = "加群记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(GroupMember groupMember)
    {
        return toAjax(groupMemberService.updateGroupMember(groupMember));
    }

    /**
     * 删除加群记录
     */
    @RequiresPermissions("app:groupMember:remove")
    @Log(title = "加群记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(groupMemberService.deleteGroupMemberByIds(ids));
    }
}
