package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.ActMember;
import com.ruoyi.app.service.IActMemberService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 报名记录Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/actMember")
public class ActMemberController extends BaseController
{
    private String prefix = "app/actMember";

    @Autowired
    private IActMemberService actMemberService;

    @RequiresPermissions("app:actMember:view")
    @GetMapping()
    public String actMember()
    {
        return prefix + "/actMember";
    }

    /**
     * 查询报名记录列表
     */
    @RequiresPermissions("app:actMember:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ActMember actMember)
    {
        startPage();
        List<ActMember> list = actMemberService.selectActMemberList(actMember);
        return getDataTable(list);
    }

    /**
     * 导出报名记录列表
     */
    @RequiresPermissions("app:actMember:export")
    @Log(title = "报名记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ActMember actMember)
    {
        List<ActMember> list = actMemberService.selectActMemberList(actMember);
        ExcelUtil<ActMember> util = new ExcelUtil<ActMember>(ActMember.class);
        return util.exportExcel(list, "actMember");
    }

    /**
     * 新增报名记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存报名记录
     */
    @RequiresPermissions("app:actMember:add")
    @Log(title = "报名记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ActMember actMember)
    {
        return toAjax(actMemberService.insertActMember(actMember));
    }

    /**
     * 修改报名记录
     */
    @GetMapping("/edit/{acmId}")
    public String edit(@PathVariable("acmId") String acmId, ModelMap mmap)
    {
        ActMember actMember = actMemberService.selectActMemberById(acmId);
        mmap.put("actMember", actMember);
        return prefix + "/edit";
    }

    /**
     * 修改保存报名记录
     */
    @RequiresPermissions("app:actMember:edit")
    @Log(title = "报名记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ActMember actMember)
    {
        return toAjax(actMemberService.updateActMember(actMember));
    }

    /**
     * 删除报名记录
     */
    @RequiresPermissions("app:actMember:remove")
    @Log(title = "报名记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(actMemberService.deleteActMemberByIds(ids));
    }
}
