package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.MemberVisitor;
import com.ruoyi.app.service.IMemberVisitorService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 访客Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/memberVisitor")
public class MemberVisitorController extends BaseController
{
    private String prefix = "app/memberVisitor";

    @Autowired
    private IMemberVisitorService memberVisitorService;

    @RequiresPermissions("app:memberVisitor:view")
    @GetMapping()
    public String memberVisitor()
    {
        return prefix + "/memberVisitor";
    }

    /**
     * 查询访客列表
     */
    @RequiresPermissions("app:memberVisitor:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MemberVisitor memberVisitor)
    {
        startPage();
        List<MemberVisitor> list = memberVisitorService.selectMemberVisitorList(memberVisitor);
        return getDataTable(list);
    }

    /**
     * 导出访客列表
     */
    @RequiresPermissions("app:memberVisitor:export")
    @Log(title = "访客", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MemberVisitor memberVisitor)
    {
        List<MemberVisitor> list = memberVisitorService.selectMemberVisitorList(memberVisitor);
        ExcelUtil<MemberVisitor> util = new ExcelUtil<MemberVisitor>(MemberVisitor.class);
        return util.exportExcel(list, "memberVisitor");
    }

    /**
     * 新增访客
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存访客
     */
    @RequiresPermissions("app:memberVisitor:add")
    @Log(title = "访客", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MemberVisitor memberVisitor)
    {
        return toAjax(memberVisitorService.insertMemberVisitor(memberVisitor));
    }

    /**
     * 修改访客
     */
    @GetMapping("/edit/{visitorId}")
    public String edit(@PathVariable("visitorId") Long visitorId, ModelMap mmap)
    {
        MemberVisitor memberVisitor = memberVisitorService.selectMemberVisitorById(visitorId);
        mmap.put("memberVisitor", memberVisitor);
        return prefix + "/edit";
    }

    /**
     * 修改保存访客
     */
    @RequiresPermissions("app:memberVisitor:edit")
    @Log(title = "访客", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MemberVisitor memberVisitor)
    {
        return toAjax(memberVisitorService.updateMemberVisitor(memberVisitor));
    }

    /**
     * 删除访客
     */
    @RequiresPermissions("app:memberVisitor:remove")
    @Log(title = "访客", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(memberVisitorService.deleteMemberVisitorByIds(ids));
    }
}
