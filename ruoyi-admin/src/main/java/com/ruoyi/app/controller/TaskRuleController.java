package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.TaskRule;
import com.ruoyi.app.service.ITaskRuleService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 任务定义Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/taskRule")
public class TaskRuleController extends BaseController
{
    private String prefix = "app/taskRule";

    @Autowired
    private ITaskRuleService taskRuleService;

    @RequiresPermissions("app:taskRule:view")
    @GetMapping()
    public String taskRule()
    {
        return prefix + "/taskRule";
    }

    /**
     * 查询任务定义列表
     */
    @RequiresPermissions("app:taskRule:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TaskRule taskRule)
    {
        startPage();
        List<TaskRule> list = taskRuleService.selectTaskRuleList(taskRule);
        return getDataTable(list);
    }

    /**
     * 导出任务定义列表
     */
    @RequiresPermissions("app:taskRule:export")
    @Log(title = "任务定义", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TaskRule taskRule)
    {
        List<TaskRule> list = taskRuleService.selectTaskRuleList(taskRule);
        ExcelUtil<TaskRule> util = new ExcelUtil<TaskRule>(TaskRule.class);
        return util.exportExcel(list, "taskRule");
    }

    /**
     * 新增任务定义
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存任务定义
     */
    @RequiresPermissions("app:taskRule:add")
    @Log(title = "任务定义", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TaskRule taskRule)
    {
        return toAjax(taskRuleService.insertTaskRule(taskRule));
    }

    /**
     * 修改任务定义
     */
    @GetMapping("/edit/{taskId}")
    public String edit(@PathVariable("taskId") String taskId, ModelMap mmap)
    {
        TaskRule taskRule = taskRuleService.selectTaskRuleById(taskId);
        mmap.put("taskRule", taskRule);
        return prefix + "/edit";
    }

    /**
     * 修改保存任务定义
     */
    @RequiresPermissions("app:taskRule:edit")
    @Log(title = "任务定义", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TaskRule taskRule)
    {
        return toAjax(taskRuleService.updateTaskRule(taskRule));
    }

    /**
     * 删除任务定义
     */
    @RequiresPermissions("app:taskRule:remove")
    @Log(title = "任务定义", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(taskRuleService.deleteTaskRuleByIds(ids));
    }
}
