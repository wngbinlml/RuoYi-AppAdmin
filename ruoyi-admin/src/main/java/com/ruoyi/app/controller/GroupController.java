package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.Group;
import com.ruoyi.app.service.IGroupService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 微信群Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/group")
public class GroupController extends BaseController
{
    private String prefix = "app/group";

    @Autowired
    private IGroupService groupService;

    @RequiresPermissions("app:group:view")
    @GetMapping()
    public String group()
    {
        return prefix + "/group";
    }

    /**
     * 查询微信群列表
     */
    @RequiresPermissions("app:group:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Group group)
    {
        startPage();
        List<Group> list = groupService.selectGroupList(group);
        return getDataTable(list);
    }

    /**
     * 导出微信群列表
     */
    @RequiresPermissions("app:group:export")
    @Log(title = "微信群", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Group group)
    {
        List<Group> list = groupService.selectGroupList(group);
        ExcelUtil<Group> util = new ExcelUtil<Group>(Group.class);
        return util.exportExcel(list, "group");
    }

    /**
     * 新增微信群
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存微信群
     */
    @RequiresPermissions("app:group:add")
    @Log(title = "微信群", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Group group)
    {
        return toAjax(groupService.insertGroup(group));
    }

    /**
     * 修改微信群
     */
    @GetMapping("/edit/{groupId}")
    public String edit(@PathVariable("groupId") String groupId, ModelMap mmap)
    {
        Group group = groupService.selectGroupById(groupId);
        mmap.put("group", group);
        return prefix + "/edit";
    }

    /**
     * 修改保存微信群
     */
    @RequiresPermissions("app:group:edit")
    @Log(title = "微信群", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Group group)
    {
        return toAjax(groupService.updateGroup(group));
    }

    /**
     * 删除微信群
     */
    @RequiresPermissions("app:group:remove")
    @Log(title = "微信群", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(groupService.deleteGroupByIds(ids));
    }
}
