package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.ActOffer;
import com.ruoyi.app.service.IActOfferService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 活动邀约Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/actOffer")
public class ActOfferController extends BaseController
{
    private String prefix = "app/actOffer";

    @Autowired
    private IActOfferService actOfferService;

    @RequiresPermissions("app:actOffer:view")
    @GetMapping()
    public String actOffer()
    {
        return prefix + "/actOffer";
    }

    /**
     * 查询活动邀约列表
     */
    @RequiresPermissions("app:actOffer:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ActOffer actOffer)
    {
        startPage();
        List<ActOffer> list = actOfferService.selectActOfferList(actOffer);
        return getDataTable(list);
    }

    /**
     * 导出活动邀约列表
     */
    @RequiresPermissions("app:actOffer:export")
    @Log(title = "活动邀约", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ActOffer actOffer)
    {
        List<ActOffer> list = actOfferService.selectActOfferList(actOffer);
        ExcelUtil<ActOffer> util = new ExcelUtil<ActOffer>(ActOffer.class);
        return util.exportExcel(list, "actOffer");
    }

    /**
     * 新增活动邀约
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存活动邀约
     */
    @RequiresPermissions("app:actOffer:add")
    @Log(title = "活动邀约", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ActOffer actOffer)
    {
        return toAjax(actOfferService.insertActOffer(actOffer));
    }

    /**
     * 修改活动邀约
     */
    @GetMapping("/edit/{offerId}")
    public String edit(@PathVariable("offerId") String offerId, ModelMap mmap)
    {
        ActOffer actOffer = actOfferService.selectActOfferById(offerId);
        mmap.put("actOffer", actOffer);
        return prefix + "/edit";
    }

    /**
     * 修改保存活动邀约
     */
    @RequiresPermissions("app:actOffer:edit")
    @Log(title = "活动邀约", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ActOffer actOffer)
    {
        return toAjax(actOfferService.updateActOffer(actOffer));
    }

    /**
     * 删除活动邀约
     */
    @RequiresPermissions("app:actOffer:remove")
    @Log(title = "活动邀约", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(actOfferService.deleteActOfferByIds(ids));
    }
}
