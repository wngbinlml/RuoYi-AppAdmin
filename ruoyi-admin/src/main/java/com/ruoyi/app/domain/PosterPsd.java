package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 举报对象 app_poster_psd
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class PosterPsd extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long psdId;

    /** 模板编号 */
    @Excel(name = "模板编号")
    private String psdNo;

    /** 模板内容 */
    @Excel(name = "模板内容")
    private String psdData;

    /** 是否启用0-未，1-是 */
    @Excel(name = "是否启用0-未，1-是")
    private Long isUse;

    public void setPsdId(Long psdId) 
    {
        this.psdId = psdId;
    }

    public Long getPsdId() 
    {
        return psdId;
    }
    public void setPsdNo(String psdNo) 
    {
        this.psdNo = psdNo;
    }

    public String getPsdNo() 
    {
        return psdNo;
    }
    public void setPsdData(String psdData) 
    {
        this.psdData = psdData;
    }

    public String getPsdData() 
    {
        return psdData;
    }
    public void setIsUse(Long isUse) 
    {
        this.isUse = isUse;
    }

    public Long getIsUse() 
    {
        return isUse;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("psdId", getPsdId())
            .append("psdNo", getPsdNo())
            .append("psdData", getPsdData())
            .append("createTime", getCreateTime())
            .append("isUse", getIsUse())
            .append("remark", getRemark())
            .toString();
    }
}
