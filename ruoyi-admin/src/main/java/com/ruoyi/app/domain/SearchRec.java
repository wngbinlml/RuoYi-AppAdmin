package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 搜索记录对象 app_search_rec
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class SearchRec extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long searchId;

    /** 关键词 */
    @Excel(name = "关键词")
    private String content;

    /** 搜索次数 */
    @Excel(name = "搜索次数")
    private Long searchNum;

    /** 是否首页展示0-否，1-是 */
    @Excel(name = "是否首页展示0-否，1-是")
    private Integer canShow;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    public void setSearchId(Long searchId) 
    {
        this.searchId = searchId;
    }

    public Long getSearchId() 
    {
        return searchId;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setSearchNum(Long searchNum) 
    {
        this.searchNum = searchNum;
    }

    public Long getSearchNum() 
    {
        return searchNum;
    }
    public void setCanShow(Integer canShow) 
    {
        this.canShow = canShow;
    }

    public Integer getCanShow() 
    {
        return canShow;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("searchId", getSearchId())
            .append("content", getContent())
            .append("createTime", getCreateTime())
            .append("searchNum", getSearchNum())
            .append("canShow", getCanShow())
            .append("memberId", getMemberId())
            .toString();
    }
}
