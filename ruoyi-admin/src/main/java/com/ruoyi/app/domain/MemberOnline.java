package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 会员在线对象 app_member_online
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class MemberOnline extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** online_id */
    private Long onlineId;

    /** 地点 */
    @Excel(name = "地点")
    private String addressName;

    /** 纬度 */
    @Excel(name = "纬度")
    private Double lat;

    /** 经度 */
    @Excel(name = "经度")
    private Double lng;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    /** 最近在线时间 */
    @Excel(name = "最近在线时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date onlineTime;

    /** 城市代号 */
    @Excel(name = "城市代号")
    private String cityCode;

    /** 性别 */
    @Excel(name = "性别")
    private Integer sex;

    public void setOnlineId(Long onlineId) 
    {
        this.onlineId = onlineId;
    }

    public Long getOnlineId() 
    {
        return onlineId;
    }
    public void setAddressName(String addressName) 
    {
        this.addressName = addressName;
    }

    public String getAddressName() 
    {
        return addressName;
    }
    public void setLat(Double lat) 
    {
        this.lat = lat;
    }

    public Double getLat() 
    {
        return lat;
    }
    public void setLng(Double lng) 
    {
        this.lng = lng;
    }

    public Double getLng() 
    {
        return lng;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setOnlineTime(Date onlineTime) 
    {
        this.onlineTime = onlineTime;
    }

    public Date getOnlineTime() 
    {
        return onlineTime;
    }
    public void setCityCode(String cityCode) 
    {
        this.cityCode = cityCode;
    }

    public String getCityCode() 
    {
        return cityCode;
    }
    public void setSex(Integer sex) 
    {
        this.sex = sex;
    }

    public Integer getSex() 
    {
        return sex;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("onlineId", getOnlineId())
            .append("addressName", getAddressName())
            .append("lat", getLat())
            .append("lng", getLng())
            .append("memberId", getMemberId())
            .append("onlineTime", getOnlineTime())
            .append("cityCode", getCityCode())
            .append("sex", getSex())
            .toString();
    }
}
