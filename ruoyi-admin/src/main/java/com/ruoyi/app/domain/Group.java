package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 微信群对象 app_group
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class Group extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ad_id */
    private String groupId;

    /** 群类型ID */
    @Excel(name = "群类型ID")
    private Long groupTypeId;

    /** 活动标题 */
    @Excel(name = "活动标题")
    private String groupTitle;

    /** 封面 */
    @Excel(name = "封面")
    private String coverImg;

    /** 子标题 */
    @Excel(name = "子标题")
    private String groupSubTitle;

    /** 地点 */
    @Excel(name = "地点")
    private String place;

    /** 描述 */
    @Excel(name = "描述")
    private String groupDesc;

    /** 浏览量 */
    @Excel(name = "浏览量")
    private Long pv;

    /** 活动状态0-暂存，1-提交，2-审核通过，3-审核未通过 */
    @Excel(name = "活动状态0-暂存，1-提交，2-审核通过，3-审核未通过")
    private Integer state;

    /** 发布会员ID */
    @Excel(name = "发布会员ID")
    private String publishMemberId;

    /** 分享次数 */
    @Excel(name = "分享次数")
    private Long shareNumber;

    /** 纬度(发布时获取坐标) */
    @Excel(name = "纬度(发布时获取坐标)")
    private Double lat;

    /** 经度 */
    @Excel(name = "经度")
    private Double lng;

    /** 入群二维码 */
    @Excel(name = "入群二维码")
    private String qrcode;

    /** 展示顺序 */
    @Excel(name = "展示顺序")
    private Long sortNum;

    /** 未通过原因 */
    @Excel(name = "未通过原因")
    private String notePassMsg;

    /** 参与是否需审核 */
    @Excel(name = "参与是否需审核")
    private Integer auditJoin;

    /** 消耗贡献值 */
    @Excel(name = "消耗贡献值")
    private Long contributionScore;

    /** 分享标题 */
    @Excel(name = "分享标题")
    private String shareTitle;

    /** 分享内容 */
    @Excel(name = "分享内容")
    private String shareDescription;

    /** 分享图片 */
    @Excel(name = "分享图片")
    private String shareImg;

    /** 分享链接 */
    @Excel(name = "分享链接")
    private String shareUrl;

    /** 开始时间 */
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 结束时间 */
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 城市编号 */
    @Excel(name = "城市编号")
    private String cityCode;

    /** delta对象,edit组件 */
    @Excel(name = "delta对象,edit组件")
    private String deltaObj;

    public void setGroupId(String groupId) 
    {
        this.groupId = groupId;
    }

    public String getGroupId() 
    {
        return groupId;
    }
    public void setGroupTypeId(Long groupTypeId) 
    {
        this.groupTypeId = groupTypeId;
    }

    public Long getGroupTypeId() 
    {
        return groupTypeId;
    }
    public void setGroupTitle(String groupTitle) 
    {
        this.groupTitle = groupTitle;
    }

    public String getGroupTitle() 
    {
        return groupTitle;
    }
    public void setCoverImg(String coverImg) 
    {
        this.coverImg = coverImg;
    }

    public String getCoverImg() 
    {
        return coverImg;
    }
    public void setGroupSubTitle(String groupSubTitle) 
    {
        this.groupSubTitle = groupSubTitle;
    }

    public String getGroupSubTitle() 
    {
        return groupSubTitle;
    }
    public void setPlace(String place) 
    {
        this.place = place;
    }

    public String getPlace() 
    {
        return place;
    }
    public void setGroupDesc(String groupDesc) 
    {
        this.groupDesc = groupDesc;
    }

    public String getGroupDesc() 
    {
        return groupDesc;
    }
    public void setPv(Long pv) 
    {
        this.pv = pv;
    }

    public Long getPv() 
    {
        return pv;
    }
    public void setState(Integer state) 
    {
        this.state = state;
    }

    public Integer getState() 
    {
        return state;
    }
    public void setPublishMemberId(String publishMemberId) 
    {
        this.publishMemberId = publishMemberId;
    }

    public String getPublishMemberId() 
    {
        return publishMemberId;
    }
    public void setShareNumber(Long shareNumber) 
    {
        this.shareNumber = shareNumber;
    }

    public Long getShareNumber() 
    {
        return shareNumber;
    }
    public void setLat(Double lat) 
    {
        this.lat = lat;
    }

    public Double getLat() 
    {
        return lat;
    }
    public void setLng(Double lng) 
    {
        this.lng = lng;
    }

    public Double getLng() 
    {
        return lng;
    }
    public void setQrcode(String qrcode) 
    {
        this.qrcode = qrcode;
    }

    public String getQrcode() 
    {
        return qrcode;
    }
    public void setSortNum(Long sortNum) 
    {
        this.sortNum = sortNum;
    }

    public Long getSortNum() 
    {
        return sortNum;
    }
    public void setNotePassMsg(String notePassMsg) 
    {
        this.notePassMsg = notePassMsg;
    }

    public String getNotePassMsg() 
    {
        return notePassMsg;
    }
    public void setAuditJoin(Integer auditJoin) 
    {
        this.auditJoin = auditJoin;
    }

    public Integer getAuditJoin() 
    {
        return auditJoin;
    }
    public void setContributionScore(Long contributionScore) 
    {
        this.contributionScore = contributionScore;
    }

    public Long getContributionScore() 
    {
        return contributionScore;
    }
    public void setShareTitle(String shareTitle) 
    {
        this.shareTitle = shareTitle;
    }

    public String getShareTitle() 
    {
        return shareTitle;
    }
    public void setShareDescription(String shareDescription) 
    {
        this.shareDescription = shareDescription;
    }

    public String getShareDescription() 
    {
        return shareDescription;
    }
    public void setShareImg(String shareImg) 
    {
        this.shareImg = shareImg;
    }

    public String getShareImg() 
    {
        return shareImg;
    }
    public void setShareUrl(String shareUrl) 
    {
        this.shareUrl = shareUrl;
    }

    public String getShareUrl() 
    {
        return shareUrl;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setCityCode(String cityCode) 
    {
        this.cityCode = cityCode;
    }

    public String getCityCode() 
    {
        return cityCode;
    }
    public void setDeltaObj(String deltaObj) 
    {
        this.deltaObj = deltaObj;
    }

    public String getDeltaObj() 
    {
        return deltaObj;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("groupId", getGroupId())
            .append("groupTypeId", getGroupTypeId())
            .append("groupTitle", getGroupTitle())
            .append("coverImg", getCoverImg())
            .append("groupSubTitle", getGroupSubTitle())
            .append("place", getPlace())
            .append("groupDesc", getGroupDesc())
            .append("pv", getPv())
            .append("state", getState())
            .append("publishMemberId", getPublishMemberId())
            .append("shareNumber", getShareNumber())
            .append("lat", getLat())
            .append("lng", getLng())
            .append("qrcode", getQrcode())
            .append("sortNum", getSortNum())
            .append("createTime", getCreateTime())
            .append("notePassMsg", getNotePassMsg())
            .append("auditJoin", getAuditJoin())
            .append("contributionScore", getContributionScore())
            .append("shareTitle", getShareTitle())
            .append("shareDescription", getShareDescription())
            .append("shareImg", getShareImg())
            .append("shareUrl", getShareUrl())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("cityCode", getCityCode())
            .append("deltaObj", getDeltaObj())
            .toString();
    }
}
