package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 活动对象 app_activity
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class Activity extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ad_id */
    private String actId;

    /** 活动类型 */
    @Excel(name = "活动类型")
    private Long actTypeId;

    /** 活动标题 */
    @Excel(name = "活动标题")
    private String actTitle;

    /** 子标题 */
    @Excel(name = "子标题")
    private String actSubTitle;

    /** 开始时间 */
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 结束时间 */
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 活动封面 */
    @Excel(name = "活动封面")
    private String coverImg;

    /** 照片集 */
    @Excel(name = "照片集")
    private String adImg;

    /** 价钱单位元 */
    @Excel(name = "价钱单位元")
    private Double price;

    /** 地点 */
    @Excel(name = "地点")
    private String place;

    /** 描述 */
    @Excel(name = "描述")
    private String actDesc;

    /** 浏览量 */
    @Excel(name = "浏览量")
    private Long pv;

    /** 回放内容 */
    @Excel(name = "回放内容")
    private String videoBackDesc;

    /** 联系人 */
    @Excel(name = "联系人")
    private String linkman;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String linkmanPhone;

    /** 活动状态0-暂存，1-提交，2-审核通过，3-审核未通过，4-结束（结束才能评价）5-取消 */
    @Excel(name = "活动状态0-暂存，1-提交，2-审核通过，3-审核未通过，4-结束", readConverterExp = "结=束才能评价")
    private Integer state;

    /** 联系微信 */
    @Excel(name = "联系微信")
    private String linkmanWx;

    /** 发布会员ID */
    @Excel(name = "发布会员ID")
    private String publishMemberId;

    /** 分享次数 */
    @Excel(name = "分享次数")
    private Long shareNumber;

    /** 纬度(发布时获取坐标) */
    @Excel(name = "纬度(发布时获取坐标)")
    private Double lat;

    /** 经度 */
    @Excel(name = "经度")
    private Double lng;

    /** 入群二维码 */
    @Excel(name = "入群二维码")
    private String qrcode;

    /** 是否加急审核0-否，1-是 */
    @Excel(name = "是否加急审核0-否，1-是")
    private Integer canUrgent;

    /** 展示顺序 */
    @Excel(name = "展示顺序")
    private Long sortNum;

    /** 暗号编号 */
    @Excel(name = "暗号编号")
    private String actCipherNo;

    /** 未通过原因 */
    @Excel(name = "未通过原因")
    private String notePassMsg;

    /** 提交时间 */
    @Excel(name = "提交时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date submitTime;

    /** 最少参与人数 */
    @Excel(name = "最少参与人数")
    private Long peopleMin;

    /** 最大参与人数 */
    @Excel(name = "最大参与人数")
    private Long peopleMax;

    /** 参与是否需审核 */
    @Excel(name = "参与是否需审核")
    private Integer auditJoin;

    /** 城市编号 */
    @Excel(name = "城市编号")
    private String cityCode;

    /** 分享标题 */
    @Excel(name = "分享标题")
    private String shareTitle;

    /** 分享内容 */
    @Excel(name = "分享内容")
    private String shareDescription;

    /** 分享图片 */
    @Excel(name = "分享图片")
    private String shareImg;

    /** 分享链接 */
    @Excel(name = "分享链接")
    private String shareUrl;

    /** 组织赏金 */
    @Excel(name = "组织赏金")
    private Double reward;

    /** delta对象,edit组件 */
    @Excel(name = "delta对象,edit组件")
    private String deltaObj;

    /** 领队趣币 */
    @Excel(name = "领队趣币")
    private Long incomeCoin;

    /** 领队酬劳 */
    @Excel(name = "领队酬劳")
    private Double incomeMoney;

    /** 是否招募领队，0-否，1-是 */
    @Excel(name = "是否招募领队，0-否，1-是")
    private Integer useLeader;

    /** 游记ID */
    @Excel(name = "游记ID")
    private String noteId;

    /** 商家ID */
    @Excel(name = "商家ID")
    private String merId;

    public void setActId(String actId) 
    {
        this.actId = actId;
    }

    public String getActId() 
    {
        return actId;
    }
    public void setActTypeId(Long actTypeId) 
    {
        this.actTypeId = actTypeId;
    }

    public Long getActTypeId() 
    {
        return actTypeId;
    }
    public void setActTitle(String actTitle) 
    {
        this.actTitle = actTitle;
    }

    public String getActTitle() 
    {
        return actTitle;
    }
    public void setActSubTitle(String actSubTitle) 
    {
        this.actSubTitle = actSubTitle;
    }

    public String getActSubTitle() 
    {
        return actSubTitle;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setCoverImg(String coverImg) 
    {
        this.coverImg = coverImg;
    }

    public String getCoverImg() 
    {
        return coverImg;
    }
    public void setAdImg(String adImg) 
    {
        this.adImg = adImg;
    }

    public String getAdImg() 
    {
        return adImg;
    }
    public void setPrice(Double price) 
    {
        this.price = price;
    }

    public Double getPrice() 
    {
        return price;
    }
    public void setPlace(String place) 
    {
        this.place = place;
    }

    public String getPlace() 
    {
        return place;
    }
    public void setActDesc(String actDesc) 
    {
        this.actDesc = actDesc;
    }

    public String getActDesc() 
    {
        return actDesc;
    }
    public void setPv(Long pv) 
    {
        this.pv = pv;
    }

    public Long getPv() 
    {
        return pv;
    }
    public void setVideoBackDesc(String videoBackDesc) 
    {
        this.videoBackDesc = videoBackDesc;
    }

    public String getVideoBackDesc() 
    {
        return videoBackDesc;
    }
    public void setLinkman(String linkman) 
    {
        this.linkman = linkman;
    }

    public String getLinkman() 
    {
        return linkman;
    }
    public void setLinkmanPhone(String linkmanPhone) 
    {
        this.linkmanPhone = linkmanPhone;
    }

    public String getLinkmanPhone() 
    {
        return linkmanPhone;
    }
    public void setState(Integer state) 
    {
        this.state = state;
    }

    public Integer getState() 
    {
        return state;
    }
    public void setLinkmanWx(String linkmanWx) 
    {
        this.linkmanWx = linkmanWx;
    }

    public String getLinkmanWx() 
    {
        return linkmanWx;
    }
    public void setPublishMemberId(String publishMemberId) 
    {
        this.publishMemberId = publishMemberId;
    }

    public String getPublishMemberId() 
    {
        return publishMemberId;
    }
    public void setShareNumber(Long shareNumber) 
    {
        this.shareNumber = shareNumber;
    }

    public Long getShareNumber() 
    {
        return shareNumber;
    }
    public void setLat(Double lat) 
    {
        this.lat = lat;
    }

    public Double getLat() 
    {
        return lat;
    }
    public void setLng(Double lng) 
    {
        this.lng = lng;
    }

    public Double getLng() 
    {
        return lng;
    }
    public void setQrcode(String qrcode) 
    {
        this.qrcode = qrcode;
    }

    public String getQrcode() 
    {
        return qrcode;
    }
    public void setCanUrgent(Integer canUrgent) 
    {
        this.canUrgent = canUrgent;
    }

    public Integer getCanUrgent() 
    {
        return canUrgent;
    }
    public void setSortNum(Long sortNum) 
    {
        this.sortNum = sortNum;
    }

    public Long getSortNum() 
    {
        return sortNum;
    }
    public void setActCipherNo(String actCipherNo) 
    {
        this.actCipherNo = actCipherNo;
    }

    public String getActCipherNo() 
    {
        return actCipherNo;
    }
    public void setNotePassMsg(String notePassMsg) 
    {
        this.notePassMsg = notePassMsg;
    }

    public String getNotePassMsg() 
    {
        return notePassMsg;
    }
    public void setSubmitTime(Date submitTime) 
    {
        this.submitTime = submitTime;
    }

    public Date getSubmitTime() 
    {
        return submitTime;
    }
    public void setPeopleMin(Long peopleMin) 
    {
        this.peopleMin = peopleMin;
    }

    public Long getPeopleMin() 
    {
        return peopleMin;
    }
    public void setPeopleMax(Long peopleMax) 
    {
        this.peopleMax = peopleMax;
    }

    public Long getPeopleMax() 
    {
        return peopleMax;
    }
    public void setAuditJoin(Integer auditJoin) 
    {
        this.auditJoin = auditJoin;
    }

    public Integer getAuditJoin() 
    {
        return auditJoin;
    }
    public void setCityCode(String cityCode) 
    {
        this.cityCode = cityCode;
    }

    public String getCityCode() 
    {
        return cityCode;
    }
    public void setShareTitle(String shareTitle) 
    {
        this.shareTitle = shareTitle;
    }

    public String getShareTitle() 
    {
        return shareTitle;
    }
    public void setShareDescription(String shareDescription) 
    {
        this.shareDescription = shareDescription;
    }

    public String getShareDescription() 
    {
        return shareDescription;
    }
    public void setShareImg(String shareImg) 
    {
        this.shareImg = shareImg;
    }

    public String getShareImg() 
    {
        return shareImg;
    }
    public void setShareUrl(String shareUrl) 
    {
        this.shareUrl = shareUrl;
    }

    public String getShareUrl() 
    {
        return shareUrl;
    }
    public void setReward(Double reward) 
    {
        this.reward = reward;
    }

    public Double getReward() 
    {
        return reward;
    }
    public void setDeltaObj(String deltaObj) 
    {
        this.deltaObj = deltaObj;
    }

    public String getDeltaObj() 
    {
        return deltaObj;
    }
    public void setIncomeCoin(Long incomeCoin) 
    {
        this.incomeCoin = incomeCoin;
    }

    public Long getIncomeCoin() 
    {
        return incomeCoin;
    }
    public void setIncomeMoney(Double incomeMoney) 
    {
        this.incomeMoney = incomeMoney;
    }

    public Double getIncomeMoney() 
    {
        return incomeMoney;
    }
    public void setUseLeader(Integer useLeader) 
    {
        this.useLeader = useLeader;
    }

    public Integer getUseLeader() 
    {
        return useLeader;
    }
    public void setNoteId(String noteId) 
    {
        this.noteId = noteId;
    }

    public String getNoteId() 
    {
        return noteId;
    }
    public void setMerId(String merId) 
    {
        this.merId = merId;
    }

    public String getMerId() 
    {
        return merId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("actId", getActId())
            .append("actTypeId", getActTypeId())
            .append("actTitle", getActTitle())
            .append("actSubTitle", getActSubTitle())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("coverImg", getCoverImg())
            .append("adImg", getAdImg())
            .append("price", getPrice())
            .append("place", getPlace())
            .append("actDesc", getActDesc())
            .append("pv", getPv())
            .append("videoBackDesc", getVideoBackDesc())
            .append("linkman", getLinkman())
            .append("linkmanPhone", getLinkmanPhone())
            .append("state", getState())
            .append("linkmanWx", getLinkmanWx())
            .append("publishMemberId", getPublishMemberId())
            .append("shareNumber", getShareNumber())
            .append("lat", getLat())
            .append("lng", getLng())
            .append("qrcode", getQrcode())
            .append("canUrgent", getCanUrgent())
            .append("sortNum", getSortNum())
            .append("actCipherNo", getActCipherNo())
            .append("createTime", getCreateTime())
            .append("notePassMsg", getNotePassMsg())
            .append("submitTime", getSubmitTime())
            .append("peopleMin", getPeopleMin())
            .append("peopleMax", getPeopleMax())
            .append("auditJoin", getAuditJoin())
            .append("cityCode", getCityCode())
            .append("shareTitle", getShareTitle())
            .append("shareDescription", getShareDescription())
            .append("shareImg", getShareImg())
            .append("shareUrl", getShareUrl())
            .append("reward", getReward())
            .append("deltaObj", getDeltaObj())
            .append("incomeCoin", getIncomeCoin())
            .append("incomeMoney", getIncomeMoney())
            .append("useLeader", getUseLeader())
            .append("noteId", getNoteId())
            .append("merId", getMerId())
            .toString();
    }
}
