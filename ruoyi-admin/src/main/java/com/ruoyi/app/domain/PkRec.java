package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * pk比赛对象 app_pk_rec
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class PkRec extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String pkId;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    /** 照片 */
    @Excel(name = "照片")
    private String headPic;

    /** 总分 */
    @Excel(name = "总分")
    private Long totalScore;

    /** 任务编号 */
    @Excel(name = "任务编号")
    private String taskId;

    /** 额外数据 */
    @Excel(name = "额外数据")
    private String dataJson;

    public void setPkId(String pkId) 
    {
        this.pkId = pkId;
    }

    public String getPkId() 
    {
        return pkId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setHeadPic(String headPic) 
    {
        this.headPic = headPic;
    }

    public String getHeadPic() 
    {
        return headPic;
    }
    public void setTotalScore(Long totalScore) 
    {
        this.totalScore = totalScore;
    }

    public Long getTotalScore() 
    {
        return totalScore;
    }
    public void setTaskId(String taskId) 
    {
        this.taskId = taskId;
    }

    public String getTaskId() 
    {
        return taskId;
    }
    public void setDataJson(String dataJson) 
    {
        this.dataJson = dataJson;
    }

    public String getDataJson() 
    {
        return dataJson;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("pkId", getPkId())
            .append("memberId", getMemberId())
            .append("createTime", getCreateTime())
            .append("headPic", getHeadPic())
            .append("totalScore", getTotalScore())
            .append("taskId", getTaskId())
            .append("dataJson", getDataJson())
            .toString();
    }
}
