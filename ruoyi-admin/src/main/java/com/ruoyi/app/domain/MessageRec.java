package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 消息记录对象 app_message_rec
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class MessageRec extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String msgId;

    /** 发送者 */
    @Excel(name = "发送者")
    private String memberId;

    /** 接收方 */
    @Excel(name = "接收方")
    private String memberIdTo;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 发送时间 */
    @Excel(name = "发送时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date sendTime;

    /** 消息类型0-普通，1-图片 */
    @Excel(name = "消息类型0-普通，1-图片")
    private Integer msgType;

    /** 状态0-待发送，1-已发送 */
    @Excel(name = "状态0-待发送，1-已发送")
    private Integer sendState;

    /** 是否读0-未，1-已读 */
    @Excel(name = "是否读0-未，1-已读")
    private Integer readState;

    public void setMsgId(String msgId) 
    {
        this.msgId = msgId;
    }

    public String getMsgId() 
    {
        return msgId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setMemberIdTo(String memberIdTo) 
    {
        this.memberIdTo = memberIdTo;
    }

    public String getMemberIdTo() 
    {
        return memberIdTo;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setSendTime(Date sendTime) 
    {
        this.sendTime = sendTime;
    }

    public Date getSendTime() 
    {
        return sendTime;
    }
    public void setMsgType(Integer msgType) 
    {
        this.msgType = msgType;
    }

    public Integer getMsgType() 
    {
        return msgType;
    }
    public void setSendState(Integer sendState) 
    {
        this.sendState = sendState;
    }

    public Integer getSendState() 
    {
        return sendState;
    }
    public void setReadState(Integer readState) 
    {
        this.readState = readState;
    }

    public Integer getReadState() 
    {
        return readState;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("msgId", getMsgId())
            .append("memberId", getMemberId())
            .append("memberIdTo", getMemberIdTo())
            .append("content", getContent())
            .append("sendTime", getSendTime())
            .append("msgType", getMsgType())
            .append("sendState", getSendState())
            .append("readState", getReadState())
            .toString();
    }
}
