package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * app版本对象 app_version_rec
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class VersionRec extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String verId;

    /** 版本号 */
    @Excel(name = "版本号")
    private String verNo;

    /** 更新内容 */
    @Excel(name = "更新内容")
    private String updateContent;

    /** 是否强制更新0-否1-是 */
    @Excel(name = "是否强制更新0-否1-是")
    private Integer canForce;

    /** sdk版本号 */
    @Excel(name = "sdk版本号")
    private String verSdkNo;

    /** 启用状态 */
    @Excel(name = "启用状态")
    private Integer canUsing;

    /** 平台类型0-h5,1-安卓,2-苹果 */
    @Excel(name = "平台类型0-h5,1-安卓,2-苹果")
    private Integer platformType;

    public void setVerId(String verId) 
    {
        this.verId = verId;
    }

    public String getVerId() 
    {
        return verId;
    }
    public void setVerNo(String verNo) 
    {
        this.verNo = verNo;
    }

    public String getVerNo() 
    {
        return verNo;
    }
    public void setUpdateContent(String updateContent) 
    {
        this.updateContent = updateContent;
    }

    public String getUpdateContent() 
    {
        return updateContent;
    }
    public void setCanForce(Integer canForce) 
    {
        this.canForce = canForce;
    }

    public Integer getCanForce() 
    {
        return canForce;
    }
    public void setVerSdkNo(String verSdkNo) 
    {
        this.verSdkNo = verSdkNo;
    }

    public String getVerSdkNo() 
    {
        return verSdkNo;
    }
    public void setCanUsing(Integer canUsing) 
    {
        this.canUsing = canUsing;
    }

    public Integer getCanUsing() 
    {
        return canUsing;
    }
    public void setPlatformType(Integer platformType) 
    {
        this.platformType = platformType;
    }

    public Integer getPlatformType() 
    {
        return platformType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("verId", getVerId())
            .append("verNo", getVerNo())
            .append("updateContent", getUpdateContent())
            .append("canForce", getCanForce())
            .append("createTime", getCreateTime())
            .append("verSdkNo", getVerSdkNo())
            .append("canUsing", getCanUsing())
            .append("platformType", getPlatformType())
            .toString();
    }
}
