package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 收藏会员对象 app_collect_member
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class CollectMember extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 收藏ID */
    private Long collectId;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    /** 收藏会员ID */
    @Excel(name = "收藏会员ID")
    private String memberIdCollect;

    public void setCollectId(Long collectId) 
    {
        this.collectId = collectId;
    }

    public Long getCollectId() 
    {
        return collectId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setMemberIdCollect(String memberIdCollect) 
    {
        this.memberIdCollect = memberIdCollect;
    }

    public String getMemberIdCollect() 
    {
        return memberIdCollect;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("collectId", getCollectId())
            .append("memberId", getMemberId())
            .append("createTime", getCreateTime())
            .append("memberIdCollect", getMemberIdCollect())
            .toString();
    }
}
