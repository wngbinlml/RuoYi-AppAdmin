package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 推荐会员对象 app_recomment_member
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class RecommentMember extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String recId;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    /** 推荐会员 */
    @Excel(name = "推荐会员")
    private String memberIdRec;

    public void setRecId(String recId) 
    {
        this.recId = recId;
    }

    public String getRecId() 
    {
        return recId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setMemberIdRec(String memberIdRec) 
    {
        this.memberIdRec = memberIdRec;
    }

    public String getMemberIdRec() 
    {
        return memberIdRec;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("recId", getRecId())
            .append("memberId", getMemberId())
            .append("memberIdRec", getMemberIdRec())
            .append("createTime", getCreateTime())
            .toString();
    }
}
