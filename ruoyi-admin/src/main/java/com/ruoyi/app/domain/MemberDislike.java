package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 不喜欢对象 app_member_dislike
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class MemberDislike extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long dislikeId;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    /** 不喜欢会员ID */
    @Excel(name = "不喜欢会员ID")
    private String memberIdDislike;

    public void setDislikeId(Long dislikeId) 
    {
        this.dislikeId = dislikeId;
    }

    public Long getDislikeId() 
    {
        return dislikeId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setMemberIdDislike(String memberIdDislike) 
    {
        this.memberIdDislike = memberIdDislike;
    }

    public String getMemberIdDislike() 
    {
        return memberIdDislike;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("dislikeId", getDislikeId())
            .append("memberId", getMemberId())
            .append("memberIdDislike", getMemberIdDislike())
            .append("createTime", getCreateTime())
            .toString();
    }
}
