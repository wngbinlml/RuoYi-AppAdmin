package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 会员基础对象 app_member_base
 * 
 * @author bigStream
 * @date 2020-08-01
 */
public class MemberBase extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 会员ID */
    private String memberId;

    /** 登录账号 */
    @Excel(name = "登录账号")
    private String loginName;

    /** 头像审核0-未审核，1-已审核 */
    @Excel(name = "头像审核0-未审核，1-已审核")
    private Long headAudit;

    /** 上首页排序 */
    @Excel(name = "上首页排序")
    private Long goHome;

    /** 头像 */
    @Excel(name = "头像")
    private String headPic;

    /** 头像封面 */
    @Excel(name = "头像封面")
    private String photoCover;

    /** 相册集 */
    @Excel(name = "相册集")
    private String photos;

    /** 用户昵称 */
    @Excel(name = "用户昵称")
    private String userName;

    /** 用户邮箱 */
    @Excel(name = "用户邮箱")
    private String email;

    /** 手机号码 */
    @Excel(name = "手机号码")
    private String mobile;

    /** 性别 */
    @Excel(name = "性别")
    private Integer sex;

    /** 密码 */
    @Excel(name = "密码")
    private String pwd;

    /** 帐号状态:0正常,1禁用 */
    @Excel(name = "帐号状态:0正常,1禁用")
    private Integer state;

    /** 最近登录时间 */
    @Excel(name = "最近登录时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date recentLoginTime;

    /** 个人签名 */
    @Excel(name = "个人签名")
    private String sign;

    /** 身高 */
    @Excel(name = "身高")
    private Long height;

    /** 体重kg */
    @Excel(name = "体重kg")
    private Long weight;

    /** 生日 */
    @Excel(name = "生日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date birthday;

    /** 绑定微信ID */
    @Excel(name = "绑定微信ID")
    private String wxId;

    /** 微信号 */
    @Excel(name = "微信号")
    private String wxNo;

    /** qq_id */
    @Excel(name = "qq_id")
    private String qqId;

    /** 支付宝ID */
    @Excel(name = "支付宝ID")
    private String alipayId;

    /** sina_id */
    @Excel(name = "sina_id")
    private String sinaId;

    /** 颜值 */
    @Excel(name = "颜值")
    private Double faceScore;

    /** 年龄 */
    @Excel(name = "年龄")
    private Long age;

    /** 爱好标签 */
    @Excel(name = "爱好标签")
    private String hobTag;

    /** 推送开关 */
    @Excel(name = "推送开关")
    private Integer notifySwitch;

    /** 设备ID */
    @Excel(name = "设备ID")
    private String deviceId;

    /** 当天颜值接口次数，每日自动清零 */
    @Excel(name = "当天颜值接口次数，每日自动清零")
    private Long faceTestNum;

    /** 注册编号 */
    @Excel(name = "注册编号")
    private Long registerNo;

    /** 注册渠道0-app;1-微信小程序；2-抖音；3-支付宝；4-h5 */
    @Excel(name = "注册渠道0-app;1-微信小程序；2-抖音；3-支付宝；4-h5")
    private Long registerChannel;

    /** 贡献值 */
    @Excel(name = "贡献值")
    private Long contributionScore;

    /** 社交价值分 */
    @Excel(name = "社交价值分")
    private Long socialScore;

    /** 平台经验值(跟等级) */
    @Excel(name = "平台经验值(跟等级)")
    private Long integralScore;

    /** 展示顺序 */
    @Excel(name = "展示顺序")
    private Long sortNum;

    /** 情感状态0-保密，1-单身，2-热恋，3-已婚 */
    @Excel(name = "情感状态0-保密，1-单身，2-热恋，3-已婚")
    private Integer affectiveState;

    /** 是否实名0-未，1-是 */
    @Excel(name = "是否实名0-未，1-是")
    private Integer canRealState;

    /** 证件类型0-身份证 */
    @Excel(name = "证件类型0-身份证")
    private Integer idType;

    /** 证件号码 */
    @Excel(name = "证件号码")
    private String idNumber;

    /** 证件姓名 */
    @Excel(name = "证件姓名")
    private String idCardName;

    /** 职业 */
    @Excel(name = "职业")
    private String postJob;

    /** 推荐人 */
    @Excel(name = "推荐人")
    private String recemMemberId;

    /** 推荐码 */
    @Excel(name = "推荐码")
    private String recemCode;

    /** 学历 */
    @Excel(name = "学历")
    private Long eduType;

    /** 年收入 */
    @Excel(name = "年收入")
    private Long yearIncome;

    /** 省编码 */
    @Excel(name = "省编码")
    private String provinceCode;

    /** 省名称 */
    @Excel(name = "省名称")
    private String provinceName;

    /** 城市编号,行政区编号 */
    @Excel(name = "城市编号,行政区编号")
    private String cityCode;

    /** 城市名称 */
    @Excel(name = "城市名称")
    private String cityName;

    /** 县区编码 */
    @Excel(name = "县区编码")
    private String districtCode;

    /** 县区名称 */
    @Excel(name = "县区名称")
    private String districtName;

    /** 省编码-设置 */
    @Excel(name = "省编码-设置")
    private String provinceCodeReal;

    /** 省名称-设置 */
    @Excel(name = "省名称-设置")
    private String provinceNameReal;

    /** 城市编号,行政区编号-设置 */
    @Excel(name = "城市编号,行政区编号-设置")
    private String cityCodeReal;

    /** 城市名称-设置 */
    @Excel(name = "城市名称-设置")
    private String cityNameReal;

    /** 县区编码-设置 */
    @Excel(name = "县区编码-设置")
    private String areaCodeReal;

    /** 县区名称-设置 */
    @Excel(name = "县区名称-设置")
    private String areaNameReal;

    /** 总趣币 */
    @Excel(name = "总趣币")
    private Long totalCoin;

    /** 领队星级 */
    @Excel(name = "领队星级")
    private Long leaderLevel;

    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setLoginName(String loginName) 
    {
        this.loginName = loginName;
    }

    public String getLoginName() 
    {
        return loginName;
    }
    public void setHeadAudit(Long headAudit) 
    {
        this.headAudit = headAudit;
    }

    public Long getHeadAudit() 
    {
        return headAudit;
    }
    public void setGoHome(Long goHome) 
    {
        this.goHome = goHome;
    }

    public Long getGoHome() 
    {
        return goHome;
    }
    public void setHeadPic(String headPic) 
    {
        this.headPic = headPic;
    }

    public String getHeadPic() 
    {
        return headPic;
    }
    public void setPhotoCover(String photoCover) 
    {
        this.photoCover = photoCover;
    }

    public String getPhotoCover() 
    {
        return photoCover;
    }
    public void setPhotos(String photos) 
    {
        this.photos = photos;
    }

    public String getPhotos() 
    {
        return photos;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setMobile(String mobile) 
    {
        this.mobile = mobile;
    }

    public String getMobile() 
    {
        return mobile;
    }
    public void setSex(Integer sex) 
    {
        this.sex = sex;
    }

    public Integer getSex() 
    {
        return sex;
    }
    public void setPwd(String pwd) 
    {
        this.pwd = pwd;
    }

    public String getPwd() 
    {
        return pwd;
    }
    public void setState(Integer state) 
    {
        this.state = state;
    }

    public Integer getState() 
    {
        return state;
    }
    public void setRecentLoginTime(Date recentLoginTime) 
    {
        this.recentLoginTime = recentLoginTime;
    }

    public Date getRecentLoginTime() 
    {
        return recentLoginTime;
    }
    public void setSign(String sign) 
    {
        this.sign = sign;
    }

    public String getSign() 
    {
        return sign;
    }
    public void setHeight(Long height) 
    {
        this.height = height;
    }

    public Long getHeight() 
    {
        return height;
    }
    public void setWeight(Long weight) 
    {
        this.weight = weight;
    }

    public Long getWeight() 
    {
        return weight;
    }
    public void setBirthday(Date birthday) 
    {
        this.birthday = birthday;
    }

    public Date getBirthday() 
    {
        return birthday;
    }
    public void setWxId(String wxId) 
    {
        this.wxId = wxId;
    }

    public String getWxId() 
    {
        return wxId;
    }
    public void setWxNo(String wxNo) 
    {
        this.wxNo = wxNo;
    }

    public String getWxNo() 
    {
        return wxNo;
    }
    public void setQqId(String qqId) 
    {
        this.qqId = qqId;
    }

    public String getQqId() 
    {
        return qqId;
    }
    public void setAlipayId(String alipayId) 
    {
        this.alipayId = alipayId;
    }

    public String getAlipayId() 
    {
        return alipayId;
    }
    public void setSinaId(String sinaId) 
    {
        this.sinaId = sinaId;
    }

    public String getSinaId() 
    {
        return sinaId;
    }
    public void setFaceScore(Double faceScore) 
    {
        this.faceScore = faceScore;
    }

    public Double getFaceScore() 
    {
        return faceScore;
    }
    public void setAge(Long age) 
    {
        this.age = age;
    }

    public Long getAge() 
    {
        return age;
    }
    public void setHobTag(String hobTag) 
    {
        this.hobTag = hobTag;
    }

    public String getHobTag() 
    {
        return hobTag;
    }
    public void setNotifySwitch(Integer notifySwitch) 
    {
        this.notifySwitch = notifySwitch;
    }

    public Integer getNotifySwitch() 
    {
        return notifySwitch;
    }
    public void setDeviceId(String deviceId) 
    {
        this.deviceId = deviceId;
    }

    public String getDeviceId() 
    {
        return deviceId;
    }
    public void setFaceTestNum(Long faceTestNum) 
    {
        this.faceTestNum = faceTestNum;
    }

    public Long getFaceTestNum() 
    {
        return faceTestNum;
    }
    public void setRegisterNo(Long registerNo) 
    {
        this.registerNo = registerNo;
    }

    public Long getRegisterNo() 
    {
        return registerNo;
    }
    public void setRegisterChannel(Long registerChannel) 
    {
        this.registerChannel = registerChannel;
    }

    public Long getRegisterChannel() 
    {
        return registerChannel;
    }
    public void setContributionScore(Long contributionScore) 
    {
        this.contributionScore = contributionScore;
    }

    public Long getContributionScore() 
    {
        return contributionScore;
    }
    public void setSocialScore(Long socialScore) 
    {
        this.socialScore = socialScore;
    }

    public Long getSocialScore() 
    {
        return socialScore;
    }
    public void setIntegralScore(Long integralScore) 
    {
        this.integralScore = integralScore;
    }

    public Long getIntegralScore() 
    {
        return integralScore;
    }
    public void setSortNum(Long sortNum) 
    {
        this.sortNum = sortNum;
    }

    public Long getSortNum() 
    {
        return sortNum;
    }
    public void setAffectiveState(Integer affectiveState) 
    {
        this.affectiveState = affectiveState;
    }

    public Integer getAffectiveState() 
    {
        return affectiveState;
    }
    public void setCanRealState(Integer canRealState) 
    {
        this.canRealState = canRealState;
    }

    public Integer getCanRealState() 
    {
        return canRealState;
    }
    public void setIdType(Integer idType) 
    {
        this.idType = idType;
    }

    public Integer getIdType() 
    {
        return idType;
    }
    public void setIdNumber(String idNumber) 
    {
        this.idNumber = idNumber;
    }

    public String getIdNumber() 
    {
        return idNumber;
    }
    public void setIdCardName(String idCardName) 
    {
        this.idCardName = idCardName;
    }

    public String getIdCardName() 
    {
        return idCardName;
    }
    public void setPostJob(String postJob) 
    {
        this.postJob = postJob;
    }

    public String getPostJob() 
    {
        return postJob;
    }
    public void setRecemMemberId(String recemMemberId) 
    {
        this.recemMemberId = recemMemberId;
    }

    public String getRecemMemberId() 
    {
        return recemMemberId;
    }
    public void setRecemCode(String recemCode) 
    {
        this.recemCode = recemCode;
    }

    public String getRecemCode() 
    {
        return recemCode;
    }
    public void setEduType(Long eduType) 
    {
        this.eduType = eduType;
    }

    public Long getEduType() 
    {
        return eduType;
    }
    public void setYearIncome(Long yearIncome) 
    {
        this.yearIncome = yearIncome;
    }

    public Long getYearIncome() 
    {
        return yearIncome;
    }
    public void setProvinceCode(String provinceCode) 
    {
        this.provinceCode = provinceCode;
    }

    public String getProvinceCode() 
    {
        return provinceCode;
    }
    public void setProvinceName(String provinceName) 
    {
        this.provinceName = provinceName;
    }

    public String getProvinceName() 
    {
        return provinceName;
    }
    public void setCityCode(String cityCode) 
    {
        this.cityCode = cityCode;
    }

    public String getCityCode() 
    {
        return cityCode;
    }
    public void setCityName(String cityName) 
    {
        this.cityName = cityName;
    }

    public String getCityName() 
    {
        return cityName;
    }
    public void setDistrictCode(String districtCode) 
    {
        this.districtCode = districtCode;
    }

    public String getDistrictCode() 
    {
        return districtCode;
    }
    public void setDistrictName(String districtName) 
    {
        this.districtName = districtName;
    }

    public String getDistrictName() 
    {
        return districtName;
    }
    public void setProvinceCodeReal(String provinceCodeReal) 
    {
        this.provinceCodeReal = provinceCodeReal;
    }

    public String getProvinceCodeReal() 
    {
        return provinceCodeReal;
    }
    public void setProvinceNameReal(String provinceNameReal) 
    {
        this.provinceNameReal = provinceNameReal;
    }

    public String getProvinceNameReal() 
    {
        return provinceNameReal;
    }
    public void setCityCodeReal(String cityCodeReal) 
    {
        this.cityCodeReal = cityCodeReal;
    }

    public String getCityCodeReal() 
    {
        return cityCodeReal;
    }
    public void setCityNameReal(String cityNameReal) 
    {
        this.cityNameReal = cityNameReal;
    }

    public String getCityNameReal() 
    {
        return cityNameReal;
    }
    public void setAreaCodeReal(String areaCodeReal) 
    {
        this.areaCodeReal = areaCodeReal;
    }

    public String getAreaCodeReal() 
    {
        return areaCodeReal;
    }
    public void setAreaNameReal(String areaNameReal) 
    {
        this.areaNameReal = areaNameReal;
    }

    public String getAreaNameReal() 
    {
        return areaNameReal;
    }
    public void setTotalCoin(Long totalCoin) 
    {
        this.totalCoin = totalCoin;
    }

    public Long getTotalCoin() 
    {
        return totalCoin;
    }
    public void setLeaderLevel(Long leaderLevel) 
    {
        this.leaderLevel = leaderLevel;
    }

    public Long getLeaderLevel() 
    {
        return leaderLevel;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("memberId", getMemberId())
            .append("loginName", getLoginName())
            .append("headAudit", getHeadAudit())
            .append("goHome", getGoHome())
            .append("headPic", getHeadPic())
            .append("photoCover", getPhotoCover())
            .append("photos", getPhotos())
            .append("userName", getUserName())
            .append("email", getEmail())
            .append("mobile", getMobile())
            .append("sex", getSex())
            .append("pwd", getPwd())
            .append("state", getState())
            .append("createTime", getCreateTime())
            .append("recentLoginTime", getRecentLoginTime())
            .append("sign", getSign())
            .append("height", getHeight())
            .append("weight", getWeight())
            .append("birthday", getBirthday())
            .append("wxId", getWxId())
            .append("wxNo", getWxNo())
            .append("qqId", getQqId())
            .append("alipayId", getAlipayId())
            .append("sinaId", getSinaId())
            .append("faceScore", getFaceScore())
            .append("age", getAge())
            .append("hobTag", getHobTag())
            .append("notifySwitch", getNotifySwitch())
            .append("deviceId", getDeviceId())
            .append("faceTestNum", getFaceTestNum())
            .append("registerNo", getRegisterNo())
            .append("registerChannel", getRegisterChannel())
            .append("contributionScore", getContributionScore())
            .append("socialScore", getSocialScore())
            .append("integralScore", getIntegralScore())
            .append("sortNum", getSortNum())
            .append("affectiveState", getAffectiveState())
            .append("canRealState", getCanRealState())
            .append("idType", getIdType())
            .append("idNumber", getIdNumber())
            .append("idCardName", getIdCardName())
            .append("postJob", getPostJob())
            .append("recemMemberId", getRecemMemberId())
            .append("recemCode", getRecemCode())
            .append("eduType", getEduType())
            .append("yearIncome", getYearIncome())
            .append("provinceCode", getProvinceCode())
            .append("provinceName", getProvinceName())
            .append("cityCode", getCityCode())
            .append("cityName", getCityName())
            .append("districtCode", getDistrictCode())
            .append("districtName", getDistrictName())
            .append("provinceCodeReal", getProvinceCodeReal())
            .append("provinceNameReal", getProvinceNameReal())
            .append("cityCodeReal", getCityCodeReal())
            .append("cityNameReal", getCityNameReal())
            .append("areaCodeReal", getAreaCodeReal())
            .append("areaNameReal", getAreaNameReal())
            .append("totalCoin", getTotalCoin())
            .append("leaderLevel", getLeaderLevel())
            .toString();
    }
}
