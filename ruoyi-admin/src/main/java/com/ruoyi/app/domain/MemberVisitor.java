package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 访客对象 app_member_visitor
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class MemberVisitor extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long visitorId;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    /** 被访问会员ID */
    @Excel(name = "被访问会员ID")
    private String memberIdVisitor;

    public void setVisitorId(Long visitorId) 
    {
        this.visitorId = visitorId;
    }

    public Long getVisitorId() 
    {
        return visitorId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setMemberIdVisitor(String memberIdVisitor) 
    {
        this.memberIdVisitor = memberIdVisitor;
    }

    public String getMemberIdVisitor() 
    {
        return memberIdVisitor;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("visitorId", getVisitorId())
            .append("memberId", getMemberId())
            .append("memberIdVisitor", getMemberIdVisitor())
            .append("createTime", getCreateTime())
            .toString();
    }
}
