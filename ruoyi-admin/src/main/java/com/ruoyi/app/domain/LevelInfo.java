package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 等级对象 app_level_info
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class LevelInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String levelId;

    /** 名称 */
    @Excel(name = "名称")
    private String title;

    /** 小标题 */
    @Excel(name = "小标题")
    private String subTitle;

    /** 级别图标 */
    @Excel(name = "级别图标")
    private String icon;

    /** 最小积分 */
    @Excel(name = "最小积分")
    private Long minScore;

    /** 最大积分 */
    @Excel(name = "最大积分")
    private Long maxScore;

    /** 备注 */
    @Excel(name = "备注")
    private String levelDesc;

    public void setLevelId(String levelId) 
    {
        this.levelId = levelId;
    }

    public String getLevelId() 
    {
        return levelId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setSubTitle(String subTitle) 
    {
        this.subTitle = subTitle;
    }

    public String getSubTitle() 
    {
        return subTitle;
    }
    public void setIcon(String icon) 
    {
        this.icon = icon;
    }

    public String getIcon() 
    {
        return icon;
    }
    public void setMinScore(Long minScore) 
    {
        this.minScore = minScore;
    }

    public Long getMinScore() 
    {
        return minScore;
    }
    public void setMaxScore(Long maxScore) 
    {
        this.maxScore = maxScore;
    }

    public Long getMaxScore() 
    {
        return maxScore;
    }
    public void setLevelDesc(String levelDesc) 
    {
        this.levelDesc = levelDesc;
    }

    public String getLevelDesc() 
    {
        return levelDesc;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("levelId", getLevelId())
            .append("title", getTitle())
            .append("subTitle", getSubTitle())
            .append("icon", getIcon())
            .append("minScore", getMinScore())
            .append("maxScore", getMaxScore())
            .append("createTime", getCreateTime())
            .append("levelDesc", getLevelDesc())
            .toString();
    }
}
