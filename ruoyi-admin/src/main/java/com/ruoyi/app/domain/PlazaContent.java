package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 广场动态对象 app_plaza_content
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class PlazaContent extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** content_id */
    private String contentId;

    /** 照片集 */
    @Excel(name = "照片集")
    private String photos;

    /** 地点 */
    @Excel(name = "地点")
    private String place;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 活动状态0-审核未通过，1-审核通过 */
    @Excel(name = "活动状态0-审核未通过，1-审核通过")
    private Integer state;

    /** 发布会员ID */
    @Excel(name = "发布会员ID")
    private String publishMemberId;

    /** 分享次数 */
    @Excel(name = "分享次数")
    private Long shareNumber;

    /** 纬度(发布时获取坐标) */
    @Excel(name = "纬度(发布时获取坐标)")
    private Double lat;

    /** 经度 */
    @Excel(name = "经度")
    private Double lng;

    /** 城市编号 */
    @Excel(name = "城市编号")
    private String cityCode;

    /** 点赞数 */
    @Excel(name = "点赞数")
    private Long supportNum;

    /** 标签 */
    @Excel(name = "标签")
    private String aiTag;

    /** 分享标题 */
    @Excel(name = "分享标题")
    private String shareTitle;

    /** 分享内容 */
    @Excel(name = "分享内容")
    private String shareDescription;

    /** 分享图片 */
    @Excel(name = "分享图片")
    private String shareImg;

    /** 分享链接 */
    @Excel(name = "分享链接")
    private String shareUrl;

    public void setContentId(String contentId) 
    {
        this.contentId = contentId;
    }

    public String getContentId() 
    {
        return contentId;
    }
    public void setPhotos(String photos) 
    {
        this.photos = photos;
    }

    public String getPhotos() 
    {
        return photos;
    }
    public void setPlace(String place) 
    {
        this.place = place;
    }

    public String getPlace() 
    {
        return place;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setState(Integer state) 
    {
        this.state = state;
    }

    public Integer getState() 
    {
        return state;
    }
    public void setPublishMemberId(String publishMemberId) 
    {
        this.publishMemberId = publishMemberId;
    }

    public String getPublishMemberId() 
    {
        return publishMemberId;
    }
    public void setShareNumber(Long shareNumber) 
    {
        this.shareNumber = shareNumber;
    }

    public Long getShareNumber() 
    {
        return shareNumber;
    }
    public void setLat(Double lat) 
    {
        this.lat = lat;
    }

    public Double getLat() 
    {
        return lat;
    }
    public void setLng(Double lng) 
    {
        this.lng = lng;
    }

    public Double getLng() 
    {
        return lng;
    }
    public void setCityCode(String cityCode) 
    {
        this.cityCode = cityCode;
    }

    public String getCityCode() 
    {
        return cityCode;
    }
    public void setSupportNum(Long supportNum) 
    {
        this.supportNum = supportNum;
    }

    public Long getSupportNum() 
    {
        return supportNum;
    }
    public void setAiTag(String aiTag) 
    {
        this.aiTag = aiTag;
    }

    public String getAiTag() 
    {
        return aiTag;
    }
    public void setShareTitle(String shareTitle) 
    {
        this.shareTitle = shareTitle;
    }

    public String getShareTitle() 
    {
        return shareTitle;
    }
    public void setShareDescription(String shareDescription) 
    {
        this.shareDescription = shareDescription;
    }

    public String getShareDescription() 
    {
        return shareDescription;
    }
    public void setShareImg(String shareImg) 
    {
        this.shareImg = shareImg;
    }

    public String getShareImg() 
    {
        return shareImg;
    }
    public void setShareUrl(String shareUrl) 
    {
        this.shareUrl = shareUrl;
    }

    public String getShareUrl() 
    {
        return shareUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("contentId", getContentId())
            .append("photos", getPhotos())
            .append("place", getPlace())
            .append("content", getContent())
            .append("state", getState())
            .append("publishMemberId", getPublishMemberId())
            .append("shareNumber", getShareNumber())
            .append("lat", getLat())
            .append("lng", getLng())
            .append("createTime", getCreateTime())
            .append("cityCode", getCityCode())
            .append("supportNum", getSupportNum())
            .append("aiTag", getAiTag())
            .append("shareTitle", getShareTitle())
            .append("shareDescription", getShareDescription())
            .append("shareImg", getShareImg())
            .append("shareUrl", getShareUrl())
            .toString();
    }
}
