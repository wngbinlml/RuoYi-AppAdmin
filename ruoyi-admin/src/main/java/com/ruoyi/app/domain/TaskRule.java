package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 任务定义对象 app_task_rule
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class TaskRule extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String taskId;

    /** 名称 */
    @Excel(name = "名称")
    private String title;

    /** 小标题 */
    @Excel(name = "小标题")
    private String subTitle;

    /** 任务描述 */
    @Excel(name = "任务描述")
    private String taskDesc;

    /** 贡献值 */
    @Excel(name = "贡献值")
    private Long contributionScore;

    /** 社交价值分 */
    @Excel(name = "社交价值分")
    private Long socialScore;

    /** 平台经验值(跟等级) */
    @Excel(name = "平台经验值(跟等级)")
    private Long integralScore;

    /** 排序 */
    @Excel(name = "排序")
    private Long sortNum;

    /** 是否启用0-未，1-是 */
    @Excel(name = "是否启用0-未，1-是")
    private Integer canUse;

    /** 有效开始时间 */
    @Excel(name = "有效开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 有效结束时间 */
    @Excel(name = "有效结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 任务封面 */
    @Excel(name = "任务封面")
    private String taskCover;

    /** 是否展示0-否1-是 */
    @Excel(name = "是否展示0-否1-是")
    private Integer canShow;

    public void setTaskId(String taskId) 
    {
        this.taskId = taskId;
    }

    public String getTaskId() 
    {
        return taskId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setSubTitle(String subTitle) 
    {
        this.subTitle = subTitle;
    }

    public String getSubTitle() 
    {
        return subTitle;
    }
    public void setTaskDesc(String taskDesc) 
    {
        this.taskDesc = taskDesc;
    }

    public String getTaskDesc() 
    {
        return taskDesc;
    }
    public void setContributionScore(Long contributionScore) 
    {
        this.contributionScore = contributionScore;
    }

    public Long getContributionScore() 
    {
        return contributionScore;
    }
    public void setSocialScore(Long socialScore) 
    {
        this.socialScore = socialScore;
    }

    public Long getSocialScore() 
    {
        return socialScore;
    }
    public void setIntegralScore(Long integralScore) 
    {
        this.integralScore = integralScore;
    }

    public Long getIntegralScore() 
    {
        return integralScore;
    }
    public void setSortNum(Long sortNum) 
    {
        this.sortNum = sortNum;
    }

    public Long getSortNum() 
    {
        return sortNum;
    }
    public void setCanUse(Integer canUse) 
    {
        this.canUse = canUse;
    }

    public Integer getCanUse() 
    {
        return canUse;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setTaskCover(String taskCover) 
    {
        this.taskCover = taskCover;
    }

    public String getTaskCover() 
    {
        return taskCover;
    }
    public void setCanShow(Integer canShow) 
    {
        this.canShow = canShow;
    }

    public Integer getCanShow() 
    {
        return canShow;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("taskId", getTaskId())
            .append("title", getTitle())
            .append("subTitle", getSubTitle())
            .append("taskDesc", getTaskDesc())
            .append("createTime", getCreateTime())
            .append("contributionScore", getContributionScore())
            .append("socialScore", getSocialScore())
            .append("integralScore", getIntegralScore())
            .append("sortNum", getSortNum())
            .append("canUse", getCanUse())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("taskCover", getTaskCover())
            .append("canShow", getCanShow())
            .toString();
    }
}
